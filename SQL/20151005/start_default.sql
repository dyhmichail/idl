CREATE TABLE "taxes" (
	`_id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`id_player`	INTEGER NOT NULL DEFAULT 1,
	`id_region`	INTEGER NOT NULL DEFAULT 1,
	`id_product`	INTEGER NOT NULL DEFAULT 1,
	`tax_rate`	INTEGER NOT NULL DEFAULT 20
);
CREATE TABLE "regions" (
	`_id`	INTEGER NOT NULL UNIQUE,
	`name`	TEXT NOT NULL UNIQUE,
	`real`	INTEGER NOT NULL DEFAULT 0,
	PRIMARY KEY(_id)
);
INSERT INTO `regions` VALUES (1,'All Regions',0);
INSERT INTO `regions` VALUES (2,'Castle',1);
INSERT INTO `regions` VALUES (3,'Farm',1);
INSERT INTO `regions` VALUES (4,'Forest',1);
INSERT INTO `regions` VALUES (5,'Mine',1);
INSERT INTO `regions` VALUES (6,'Outside',0);
CREATE TABLE "products" (
	`_id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`name`	TEXT NOT NULL UNIQUE,
	`real`	INTEGER NOT NULL DEFAULT 0
);
INSERT INTO `products` VALUES (1,'All Products',1);
INSERT INTO `products` VALUES (2,'Food',1);
INSERT INTO `products` VALUES (3,'Wood',1);
INSERT INTO `products` VALUES (4,'Goods',1);
INSERT INTO `products` VALUES (5,'Stone',1);
INSERT INTO `products` VALUES (6,'Protection',0);
INSERT INTO `products` VALUES (7,'PeopleDay',0);
CREATE TABLE "parameters" (
	`_id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`name`	TEXT NOT NULL,
	`value`	TEXT NOT NULL,
	`desc`	TEXT NOT NULL,
	`active`	INTEGER NOT NULL DEFAULT 1
);
INSERT INTO `parameters` VALUES (1,'test_numeric','12345','Тестовый цифровой',1);
INSERT INTO `parameters` VALUES (2,'test_string','привет!','Тестовый строковый',1);
CREATE TABLE "buildings" (
	`_id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`name`	TEXT NOT NULL UNIQUE,
	`real`	INTEGER DEFAULT 1,
	`sprite`	TEXT,
	`can_construct`	INTEGER DEFAULT 1
);
INSERT INTO `buildings` VALUES (1,'Castle',1,'castle',0);
INSERT INTO `buildings` VALUES (2,'Farm',1,'farm',1);
INSERT INTO `buildings` VALUES (3,'Sawmill',1,'sawmill',1);
INSERT INTO `buildings` VALUES (4,'House',1,'house',1);
INSERT INTO `buildings` VALUES (5,'Workshop',1,'workshop',1);
INSERT INTO `buildings` VALUES (6,'Barracks',1,'barracks',1);
INSERT INTO `buildings` VALUES (7,'Mine',1,'mine',1);
INSERT INTO `buildings` VALUES (8,'Patrol',0,NULL,0);
INSERT INTO `buildings` VALUES (9,'Construct',0,NULL,0);
