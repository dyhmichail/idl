PRAGMA writable_schema = 1;
delete from sqlite_master where type in ('table', 'index', 'trigger');
PRAGMA writable_schema = 0;
VACUUM;
PRAGMA INTEGRITY_CHECK;


CREATE TABLE "people_transfer_reasons" (
	'_id'	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	'name'	TEXT NOT NULL UNIQUE
);
INSERT INTO 'people_transfer_reasons'( _id, name ) VALUES ( 1, 'ManualAdd');
INSERT INTO 'people_transfer_reasons'( _id, name ) VALUES ( 2, 'FromOutside');
INSERT INTO 'people_transfer_reasons'( _id, name ) VALUES ( 3, 'RandomTransfer');
INSERT INTO 'people_transfer_reasons'( _id, name ) VALUES ( 4, 'FindWorkInAnotherRegion');
INSERT INTO 'people_transfer_reasons'( _id, name ) VALUES ( 5, 'FindRegionWithFood');
INSERT INTO 'people_transfer_reasons'( _id, name ) VALUES ( 6, 'FindRegionWithGoods');
INSERT INTO 'people_transfer_reasons'( _id, name ) VALUES ( 7, 'FindRegionWithProtect');

CREATE TABLE "slots" (
	'_id'     INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	'name'    TEXT NOT NULL UNIQUE,
	'color_r' INTEGER NOT NULL,
	'color_g' INTEGER NOT NULL,
	'color_b' INTEGER NOT NULL
);
INSERT INTO 'slots'( _id, name, color_r, color_g, color_b) VALUES ( 1, 'Free',               0, 0, 1);
INSERT INTO 'slots'( _id, name, color_r, color_g, color_b) VALUES ( 2, 'Farmer',             1, 1, 0);
INSERT INTO 'slots'( _id, name, color_r, color_g, color_b) VALUES ( 3, 'Lumberjack',         0, 1, 0);
INSERT INTO 'slots'( _id, name, color_r, color_g, color_b) VALUES ( 4, 'Worker',             0, 0, 1);
INSERT INTO 'slots'( _id, name, color_r, color_g, color_b) VALUES ( 5, 'Soldier',            1, 0, 1);
INSERT INTO 'slots'( _id, name, color_r, color_g, color_b) VALUES ( 6, 'Miner',              1, 0, 0);
INSERT INTO 'slots'( _id, name, color_r, color_g, color_b) VALUES ( 7, 'ConstructionWorker', 1, 1, 1);

CREATE TABLE "regions" (
	'_id'	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	'name'	TEXT NOT NULL UNIQUE,
	'real'	INTEGER NOT NULL DEFAULT 0,
	'time_to_delivery' INTEGER NOT NULL DEFAULT 1
	
);
INSERT INTO 'regions'(_id, name, real, time_to_delivery) VALUES ( 1, 'Castle',      1, 0);
INSERT INTO 'regions'(_id, name, real, time_to_delivery) VALUES ( 2, 'Farm',        1, 3);
INSERT INTO 'regions'(_id, name, real, time_to_delivery) VALUES ( 3, 'Forest',      1, 4);
INSERT INTO 'regions'(_id, name, real, time_to_delivery) VALUES ( 4, 'Mine',        1, 5);
INSERT INTO 'regions'(_id, name, real, time_to_delivery) VALUES ( 5, 'Outside',     0, 5);
INSERT INTO 'regions'(_id, name, real, time_to_delivery) VALUES ( 6, 'Player',      0, 5);

CREATE TABLE "buildings" (
	'_id'		 INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	'name'		 TEXT NOT NULL UNIQUE,
	'real'		 INTEGER NOT NULL DEFAULT 1,
	'seats_quantity' INTEGER NOT NULL DEFAULT 2,
	'time_to_build'  INTEGER NOT NULL DEFAULT 10
);
INSERT INTO 'buildings'( _id, name, real, seats_quantity, time_to_build) VALUES ( 1, 'Castle',    0,  0, 10);
INSERT INTO 'buildings'( _id, name, real, seats_quantity, time_to_build) VALUES ( 2, 'Farm',      1,  2, 10);
INSERT INTO 'buildings'( _id, name, real, seats_quantity, time_to_build) VALUES ( 3, 'Sawmill',   1,  4, 20);
INSERT INTO 'buildings'( _id, name, real, seats_quantity, time_to_build) VALUES ( 4, 'House',     1,  0, 10);
INSERT INTO 'buildings'( _id, name, real, seats_quantity, time_to_build) VALUES ( 5, 'Workshop',  1,  2, 15);
INSERT INTO 'buildings'( _id, name, real, seats_quantity, time_to_build) VALUES ( 6, 'Barracks',  1,  0, 10);
INSERT INTO 'buildings'( _id, name, real, seats_quantity, time_to_build) VALUES ( 7, 'Mine',      1,  3, 10);
INSERT INTO 'buildings'( _id, name, real, seats_quantity, time_to_build) VALUES ( 8, 'Patrol',    0, 99, 10);
INSERT INTO 'buildings'( _id, name, real, seats_quantity, time_to_build) VALUES ( 9, 'Construct', 0,  0, 10);

CREATE TABLE "products" (
	'_id'	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	'name'	TEXT NOT NULL UNIQUE,
	'real'	INTEGER NOT NULL DEFAULT 0
);
INSERT INTO 'products'( _id, name, real) VALUES ( 1, 'Food',         1);
INSERT INTO 'products'( _id, name, real) VALUES ( 2, 'Wood',         1);
INSERT INTO 'products'( _id, name, real) VALUES ( 3, 'Goods',        1);
INSERT INTO 'products'( _id, name, real) VALUES ( 4, 'Ore',          1);
INSERT INTO 'products'( _id, name, real) VALUES ( 5, 'Weapons',      1);
INSERT INTO 'products'( _id, name, real) VALUES ( 6, 'Protection',   0);
INSERT INTO 'products'( _id, name, real) VALUES ( 7, 'PeopleDay',    0);

CREATE TABLE "goods" (
	'_id'	      INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	'name'	      TEXT NOT NULL UNIQUE,
	'real'	      INTEGER NOT NULL DEFAULT 0,
	'id_products' INTEGER NOT NULL,
	'id_slots'    INTEGER NOT NULL,
	FOREIGN KEY(id_products) REFERENCES products(_id),
	FOREIGN KEY(id_slots) REFERENCES slots(_id)
);
INSERT INTO 'goods'( _id, name, real, id_products, id_slots) VALUES ( 1, 'grain',      1, 1, 2);
INSERT INTO 'goods'( _id, name, real, id_products, id_slots) VALUES ( 2, 'pork',       1, 1, 2);
INSERT INTO 'goods'( _id, name, real, id_products, id_slots) VALUES ( 3, 'usual_wood', 1, 2, 3);
INSERT INTO 'goods'( _id, name, real, id_products, id_slots) VALUES ( 4, 'money',      1, 3, 4);
INSERT INTO 'goods'( _id, name, real, id_products, id_slots) VALUES ( 5, 'gold',       1, 4, 6);
INSERT INTO 'goods'( _id, name, real, id_products, id_slots) VALUES ( 6, 'silver',     1, 4, 6);
INSERT INTO 'goods'( _id, name, real, id_products, id_slots) VALUES ( 7, 'stone',      1, 4, 6);
INSERT INTO 'goods'( _id, name, real, id_products, id_slots) VALUES ( 8, 'weapon',     1, 5, 4);
INSERT INTO 'goods'( _id, name, real, id_products, id_slots) VALUES ( 9, 'protection', 0, 6, 5);
INSERT INTO 'goods'( _id, name, real, id_products, id_slots) VALUES (10, 'jewelery',   1, 3, 4);
INSERT INTO 'goods'( _id, name, real, id_products, id_slots) VALUES (11, 'food',       0, 1, 2);
INSERT INTO 'goods'( _id, name, real, id_products, id_slots) VALUES (12, 'goods',      0, 4, 6);

CREATE TABLE "good_need" (
	'_id'	   INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	'id_good_produce' INTEGER NOT NULL,
	'id_good_need'    INTEGER NOT NULL,
	'quantity'        INTEGER NOT NULL,
	FOREIGN KEY(id_good_produce) REFERENCES goods(_id),
	FOREIGN KEY(id_good_need) REFERENCES goods(_id)
);
INSERT INTO 'good_need'( _id, id_good_produce, id_good_need, quantity) VALUES (1, 4, 5, 10);
INSERT INTO 'good_need'( _id, id_good_produce, id_good_need, quantity) VALUES (2, 8, 3, 10);
INSERT INTO 'good_need'( _id, id_good_produce, id_good_need, quantity) VALUES (3, 8, 7, 10);


CREATE TABLE "need_4_build" (
	'_id'	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	'id_buildings'	INTEGER NOT NULL,
	'id_goods'	INTEGER NOT NULL,
	'quantity'	INTEGER NOT NULL DEFAULT 0,
	FOREIGN KEY(id_goods) REFERENCES goods(_id),
	FOREIGN KEY(id_buildings) REFERENCES buildings(_id)
);
INSERT INTO 'need_4_build'(id_buildings, id_goods, quantity) VALUES ( 3, 3, 80);
INSERT INTO 'need_4_build'(id_buildings, id_goods, quantity) VALUES ( 3, 7, 80);
INSERT INTO 'need_4_build'(id_buildings, id_goods, quantity) VALUES ( 5, 3, 80);
INSERT INTO 'need_4_build'(id_buildings, id_goods, quantity) VALUES ( 5, 7, 80);

CREATE TABLE "building_produce" (
	'_id'	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	'id_buildings'	INTEGER NOT NULL,
	'id_goods'	INTEGER NOT NULL,
	'per_seat'	INTEGER NOT NULL DEFAULT 1,
	FOREIGN KEY(id_goods) REFERENCES goods(_id),
	FOREIGN KEY(id_buildings) REFERENCES buildings(_id)
);
INSERT INTO 'building_produce'(id_buildings, id_goods, per_seat) VALUES ( 2, 1, 10);
INSERT INTO 'building_produce'(id_buildings, id_goods, per_seat) VALUES ( 2, 2, 10);
INSERT INTO 'building_produce'(id_buildings, id_goods, per_seat) VALUES ( 3, 3, 10);
INSERT INTO 'building_produce'(id_buildings, id_goods, per_seat) VALUES ( 5, 4, 10);
INSERT INTO 'building_produce'(id_buildings, id_goods, per_seat) VALUES ( 5, 8, 10);
INSERT INTO 'building_produce'(id_buildings, id_goods, per_seat) VALUES ( 7, 5, 10);
INSERT INTO 'building_produce'(id_buildings, id_goods, per_seat) VALUES ( 7, 6, 10);
INSERT INTO 'building_produce'(id_buildings, id_goods, per_seat) VALUES ( 7, 7, 10);
INSERT INTO 'building_produce'(id_buildings, id_goods, per_seat) VALUES ( 8, 9, 10);

CREATE TABLE "start_params" (
	'_id'	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	'name'	TEXT NOT NULL,
	'value' INTEGER NOT NULL DEFAULT 0,
	'real'  INTEGER NOT NULL DEFAULT 1,
	'desc'	TEXT NOT NULL
);
INSERT INTO 'start_params'( _id, real, name, value, desc) VALUES (  1, 1, 'init_population',                 14, '����������� ���������');
INSERT INTO 'start_params'( _id, real, name, value, desc) VALUES (  2, 1, 'max_population',                  28, '������������ ���������');
INSERT INTO 'start_params'( _id, real, name, value, desc) VALUES (  3, 1, 'add_people_per_food_quantity',     5, '�� ����� ������� ��� ����� ��������� ������� �����');
INSERT INTO 'start_params'( _id, real, name, value, desc) VALUES (  4, 1, 'min_goods_to_transfer',           20, '����������� ������ ������ ��� �����������');
INSERT INTO 'start_params'( _id, real, name, value, desc) VALUES (  5, 1, 'percent_extra_goods_to_transfer', 50, '������� ����������� ������� ������ � �������� ������ � ������ � ���������� ���������� ������ ������');
INSERT INTO 'start_params'( _id, real, name, value, desc) VALUES (  6, 1, 'eat_per_tour',                     1, '������� ��� ��� ���� ������� �� ���');
INSERT INTO 'start_params'( _id, real, name, value, desc) VALUES (  7, 1, 'use_goods_per_tour',               2, '������� ���� ���������� ���� ������� �� ���');
INSERT INTO 'start_params'( _id, real, name, value, desc) VALUES (  8, 1, 'need_protection_per_tour',         3, '������� ������ ������������ ���������� �������� �� ���');
INSERT INTO 'start_params'( _id, real, name, value, desc) VALUES (  9, 1, 'teac_duration',                    1, '����������������� ���� � ��������');

INSERT INTO 'start_params'( _id, real, name, value, desc) VALUES ( 10, 1, 'food_limit',                       1, '');
INSERT INTO 'start_params'( _id, real, name, value, desc) VALUES ( 11, 1, 'food_limit_now',                   1, '');
INSERT INTO 'start_params'( _id, real, name, value, desc) VALUES ( 12, 1, 'food_limit_random_min',            1, '');
INSERT INTO 'start_params'( _id, real, name, value, desc) VALUES ( 13, 1, 'food_limit_random_max',            1, '');
INSERT INTO 'start_params'( _id, real, name, value, desc) VALUES ( 14, 1, 'protect_limit',                    1, '');
INSERT INTO 'start_params'( _id, real, name, value, desc) VALUES ( 15, 1, 'protect_limit_now',                1, '');
INSERT INTO 'start_params'( _id, real, name, value, desc) VALUES ( 16, 1, 'protect_limit_random_min',         1, '');
INSERT INTO 'start_params'( _id, real, name, value, desc) VALUES ( 17, 1, 'protect_limit_random_max',         1, '');
INSERT INTO 'start_params'( _id, real, name, value, desc) VALUES ( 18, 1, 'goods_limit',                      1, '');
INSERT INTO 'start_params'( _id, real, name, value, desc) VALUES ( 19, 1, 'goods_limit_now',                  1, '');
INSERT INTO 'start_params'( _id, real, name, value, desc) VALUES ( 20, 1, 'goods_limit_random_min',           1, '');
INSERT INTO 'start_params'( _id, real, name, value, desc) VALUES ( 21, 1, 'goods_limit_random_max',           1, '');

INSERT INTO 'start_params'( _id, real, name, value, desc) VALUES ( 22, 1, 'min_army',                         5, '����������� ������ �����');
INSERT INTO 'start_params'( _id, real, name, value, desc) VALUES ( 23, 1, 'percent_random_people_moving',    20, '������� ���������� ����������� ��������� ����� ����� ���������');
