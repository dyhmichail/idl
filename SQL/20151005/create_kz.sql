PRAGMA writable_schema = 1;
delete from sqlite_master where type in ('table', 'index', 'trigger');
PRAGMA writable_schema = 0;
VACUUM;
PRAGMA INTEGRITY_CHECK;


CREATE TABLE `h-42419_idl`.`people_transfer_reasons` ( 
	`_id` INT(2) NOT NULL AUTO_INCREMENT, 
	`name` TEXT NOT NULL,
	PRIMARY KEY (`_id`))
	ENGINE = InnoDB; 

INSERT INTO `people_transfer_reasons`( _id, name ) VALUES ( 1, 'ManualAdd');
INSERT INTO `people_transfer_reasons`( _id, name ) VALUES ( 2, 'FromOutside');
INSERT INTO `people_transfer_reasons`( _id, name ) VALUES ( 3, 'RandomTransfer');
INSERT INTO `people_transfer_reasons`( _id, name ) VALUES ( 4, 'FindWorkInAnotherRegion');
INSERT INTO `people_transfer_reasons`( _id, name ) VALUES ( 5, 'FindRegionWithFood');
INSERT INTO `people_transfer_reasons`( _id, name ) VALUES ( 6, 'FindRegionWithGoods');
INSERT INTO `people_transfer_reasons`( _id, name ) VALUES ( 7, 'FindRegionWithProtect');


CREATE TABLE `h-42419_idl`.`slots` ( 
	`_id` INT(2) NOT NULL AUTO_INCREMENT, 
	`name` TEXT NOT NULL,
	PRIMARY KEY (`_id`))
	ENGINE = InnoDB; 

INSERT INTO `slots`( _id, name) VALUES ( 1, 'Free');
INSERT INTO `slots`( _id, name) VALUES ( 2, 'Farmer');
INSERT INTO `slots`( _id, name) VALUES ( 3, 'Lumberjack');
INSERT INTO `slots`( _id, name) VALUES ( 4, 'Worker');
INSERT INTO `slots`( _id, name) VALUES ( 5, 'Soldier');
INSERT INTO `slots`( _id, name) VALUES ( 6, 'Miner');
INSERT INTO `slots`( _id, name) VALUES ( 7, 'ConstructionWorker');


CREATE TABLE "regions" (
	`_id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`name`	TEXT NOT NULL UNIQUE,
	`real`	INTEGER NOT NULL DEFAULT 0,
	'time_to_delivery' INTEGER NOT NULL DEFAULT 1
	
);
INSERT INTO `regions`(_id, name, real, time_to_delivery) VALUES ( 1, 'All Regions',0, 0);
INSERT INTO `regions`(_id, name, real, time_to_delivery) VALUES ( 2, 'Castle',1, 0);
INSERT INTO `regions`(_id, name, real, time_to_delivery) VALUES ( 3, 'Farm',1, 3);
INSERT INTO `regions`(_id, name, real, time_to_delivery) VALUES ( 4, 'Forest',1, 4);
INSERT INTO `regions`(_id, name, real, time_to_delivery) VALUES ( 5, 'Mine',1, 5);
INSERT INTO `regions`(_id, name, real, time_to_delivery) VALUES ( 6, 'Outside',0, 5);

CREATE TABLE "buildings" (
	`_id`			INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`name`			TEXT NOT NULL UNIQUE,
	`real`			INTEGER NOT NULL DEFAULT 1,
	`seats_quantity`		INTEGER NOT NULL DEFAULT 2
);
INSERT INTO `buildings`( _id, name, real, seats_quantity) VALUES ( 1, 'Castle',    0,  0);
INSERT INTO `buildings`( _id, name, real, seats_quantity) VALUES ( 2, 'Farm', 	  1,  2);
INSERT INTO `buildings`( _id, name, real, seats_quantity) VALUES ( 3, 'Sawmill',   1,  4);
INSERT INTO `buildings`( _id, name, real, seats_quantity) VALUES ( 4, 'House',	  1,  0);
INSERT INTO `buildings`( _id, name, real, seats_quantity) VALUES ( 5, 'Workshop',  1,  2);
INSERT INTO `buildings`( _id, name, real, seats_quantity) VALUES ( 6, 'Barracks',  1,  0);
INSERT INTO `buildings`( _id, name, real, seats_quantity) VALUES ( 7, 'Mine',	  1,  3);
INSERT INTO `buildings`( _id, name, real, seats_quantity) VALUES ( 8, 'Patrol',    0, 99);
INSERT INTO `buildings`( _id, name, real, seats_quantity) VALUES ( 9, 'Construct', 0,  0);

CREATE TABLE "products" (
	`_id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`name`	TEXT NOT NULL UNIQUE,
	`real`	INTEGER NOT NULL DEFAULT 0
);
INSERT INTO `products`( _id, name, real) VALUES ( 1, 'All Products', 1);
INSERT INTO `products`( _id, name, real) VALUES ( 2, 'Food',         1);
INSERT INTO `products`( _id, name, real) VALUES ( 3, 'Wood',         1);
INSERT INTO `products`( _id, name, real) VALUES ( 4, 'Goods',        1);
INSERT INTO `products`( _id, name, real) VALUES ( 5, 'Ore',          1);
INSERT INTO `products`( _id, name, real) VALUES ( 6, 'Protection',   0);
INSERT INTO `products`( _id, name, real) VALUES ( 7, 'PeopleDay',    0);

CREATE TABLE "goods" (
	`_id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`name`	TEXT NOT NULL UNIQUE,
	`real`	INTEGER NOT NULL DEFAULT 0,
	'id_products' INTEGER NOT NULL,
	FOREIGN KEY(id_products) REFERENCES products(_id)
);
INSERT INTO `goods`( _id, name, real, id_products) VALUES ( 1, 'grain',	     1, 2);
INSERT INTO `goods`( _id, name, real, id_products) VALUES ( 2, 'pork',	     1, 2);
INSERT INTO `goods`( _id, name, real, id_products) VALUES ( 3, 'usual_wood', 1, 3);
INSERT INTO `goods`( _id, name, real, id_products) VALUES ( 4, 'money',	     1, 4);
INSERT INTO `goods`( _id, name, real, id_products) VALUES ( 5, 'gold',	     1, 5);
INSERT INTO `goods`( _id, name, real, id_products) VALUES ( 6, 'silver',     1, 5);
INSERT INTO `goods`( _id, name, real, id_products) VALUES ( 7, 'stone',	     1, 5);
INSERT INTO `goods`( _id, name, real, id_products) VALUES ( 8, 'weapon',     1, 5);
INSERT INTO `goods`( _id, name, real, id_products) VALUES ( 9, 'protection', 0, 6);

CREATE TABLE "whats_need_to_construct" (
	`_id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	'id_buildings'	INTEGER NOT NULL,
	'id_products'	INTEGER NOT NULL,
	'quantity'	INTEGER NOT NULL DEFAULT 0,
	FOREIGN KEY(id_products) REFERENCES products(_id),
	FOREIGN KEY(id_buildings) REFERENCES buildings(_id)
);

INSERT INTO `whats_need_to_construct`(id_buildings, id_products, quantity) VALUES ( 3, 3, 10);
INSERT INTO `whats_need_to_construct`(id_buildings, id_products, quantity) VALUES ( 3, 7, 10);
INSERT INTO `whats_need_to_construct`(id_buildings, id_products, quantity) VALUES ( 5, 3, 10);
INSERT INTO `whats_need_to_construct`(id_buildings, id_products, quantity) VALUES ( 5, 7, 10);


CREATE TABLE "whats_building_produce" (
	`_id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	'id_buildings'	INTEGER NOT NULL,
	'id_goods'	INTEGER NOT NULL,
	`per_seat`	INTEGER NOT NULL DEFAULT 1,
	FOREIGN KEY(id_goods) REFERENCES goods(_id),
	FOREIGN KEY(id_buildings) REFERENCES buildings(_id)
);

INSERT INTO `whats_building_produce`(id_buildings, id_goods, per_seat) VALUES ( 2, 1, 10);
INSERT INTO `whats_building_produce`(id_buildings, id_goods, per_seat) VALUES ( 2, 2, 10);
INSERT INTO `whats_building_produce`(id_buildings, id_goods, per_seat) VALUES ( 3, 3, 10);
INSERT INTO `whats_building_produce`(id_buildings, id_goods, per_seat) VALUES ( 5, 4, 10);
INSERT INTO `whats_building_produce`(id_buildings, id_goods, per_seat) VALUES ( 5, 8, 10);
INSERT INTO `whats_building_produce`(id_buildings, id_goods, per_seat) VALUES ( 7, 5, 10);
INSERT INTO `whats_building_produce`(id_buildings, id_goods, per_seat) VALUES ( 7, 6, 10);
INSERT INTO `whats_building_produce`(id_buildings, id_goods, per_seat) VALUES ( 7, 7, 10);
INSERT INTO `whats_building_produce`(id_buildings, id_goods, per_seat) VALUES ( 8, 9, 10);
