-- How many pics to create
picsCount2render = 90

-- Output pic width
renderWidth = 1000

-- Output pic height
renderHeight = 1600

-- folder with OBJ
folderOBJ = "obj"

-- output folder for PNG
folderReady = "ready"

-- materials library
matLib = "matlib/clothes.mat"

-- camera name
cameraName = "VRayPhysicalCamera01"
