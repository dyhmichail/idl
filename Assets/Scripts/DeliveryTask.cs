﻿using UnityEngine;
using System.Collections;

public class DeliveryTask : MonoBehaviour {

	public static int current_ID = 1;
	public int delivery_ID;
	public Region fromDelivery;
	public Region whomDelivery;
	public GoodType typeOfGoodsForDelivery;
	public int quantytiGoodsToDelivery;
	public int timeRestForDelivery;
	public bool toDelete;
	
	
	public DeliveryTask(Region _fromDelivery, Region _whomDelivery, GoodType _typeOfGoodsForDelivery, int _quantytiGoodsToDelivery, int _timeRestForDelivery, bool _toDelete){
		delivery_ID = DeliveryTask.current_ID++;
		fromDelivery = _fromDelivery;
		whomDelivery = _whomDelivery;
		typeOfGoodsForDelivery = _typeOfGoodsForDelivery;
		quantytiGoodsToDelivery = _quantytiGoodsToDelivery;
		timeRestForDelivery = _timeRestForDelivery;
		toDelete = _toDelete;
	}

}
