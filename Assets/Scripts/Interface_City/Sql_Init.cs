﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic; 
using System.Data;
using Mono.Data.Sqlite;
using System.IO;
/// <summary>
/// GIT WORKING OK!
/// Accept and resend.
/// </summary>

public class Sql_Init : MonoBehaviour {


	public static IDbConnection _connection;
	public static IDbCommand _command;
	public static IDataReader _reader;
	public static string sql;
	public static string connection_string;
    public string test;

	public delegate void ChangeRegionName(string regionName);


	void Awake(){ 
		Init_SQL_Connection();

	} 
	
	void OnApplicationQuit() { 
		Debug.Log("Killing connection"); 
		if (_connection != null){ 
			if (_connection.State.ToString()!="Closed")
				_connection.Close(); 
			_connection.Dispose(); 
		} 
	}

	public void Init_SQL_Connection(){
		try { 
			connection_string = "URI=file:"+Application.persistentDataPath+"/idl.sqlite";
			_connection = new SqliteConnection(connection_string);
			_connection.Open();
			Debug.Log("Connection State: " + _connection.State); 
		} 
		catch (IOException e)  {Debug.Log(e.ToString());} 
	}

	/*
	public static void Init_Region_GameObjects(){
		GameObject[] AllRegions = GameObject.FindGameObjectsWithTag("GameObject_Region");
		foreach (GameObject G in AllRegions){	
			GameObject_Regions.Add(G);
		}
		Change_Region_GameObject("GameObject_Castle");
	}
*/


}
