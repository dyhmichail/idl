﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OverPeople : MonoBehaviour
{

    public static GameObject PersonalCanvas;


    public void ShowPersonalInfo()
    {
        //PersonalCanvas.SetActive(true);
        //PersonalCanvas.transform.SetParent(transform);
        //PersonalCanvas.transform.localScale = new Vector3(0.3f, 0.3f, 1);
        //PersonalCanvas.transform.localPosition = new Vector3(0, -50, 0);
        GetComponentInParent<Image>().color = new Color(1,0,0,1);
        Get_People_Form_GameObject(this.gameObject);
        gm.CurrentPeoplePersonal = this.gameObject;
    }

    public void HidePersonalInfo(){
        //PersonalCanvas.SetActive(false);
        GetComponentInParent<Image>().color = new Color(0, 1, 0, 1);
    }

    public static People Get_People_Form_GameObject(GameObject currentGameObject)
    {
        foreach (var currentRegion in gm.Regions)
        {
            foreach (var currentPeople in currentRegion.Value.Peoples)
            {
                if (currentPeople.people_GameObject == currentGameObject)
                {
                    PersonalCanvas.GetComponentInChildren<Text>().text = currentPeople.people_Name + "\n"+
                        "Food - " + currentPeople.food_limit_now + "/" + currentPeople.food_limit + "\n"+
                        "Goods - " + currentPeople.goods_limit_now + "/" + currentPeople.goods_limit + "\n"+
                        "Protect - " + currentPeople.protect_limit_now + "/" + currentPeople.protect_limit;
                }
            }
        }
        return null;
    }
}
