﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 
using System.Data;
using Mono.Data.Sqlite;
using System.IO;

public class Test_MySQL : MonoBehaviour {
	public static IDbConnection _connection;
	public static IDbCommand _command;
	public static IDataReader _reader;
	public static string sql;
	public string connection_string;

	void Start(){
		TestConnection();
	}

	void Awake(){ 
		try { 
			connection_string = "URI=file:"+Application.persistentDataPath+"/idl.sqlite";
			_connection = new SqliteConnection(connection_string);
			_connection.Open();
			Debug.Log("Connection State: " + _connection.State); 
		} 
		catch (IOException e)  {Debug.Log(e.ToString());} 
	} 
	
	void OnApplicationQuit() { 
		Debug.Log("Killing connection"); 
		if (_connection != null){ 
			if (_connection.State.ToString()!="Closed")
				_connection.Close(); 
			_connection.Dispose(); 
		} 
	}

	public void TestConnection(){
		_command = _connection.CreateCommand();
		sql = "select _id, name, value, desc, active from parameters where active = 1";
		_command.CommandText = sql;_reader = _command.ExecuteReader();
		while (_reader.Read()){
			int		cur_id	 = _reader.GetInt32(0);
			string	cur_name = _reader.GetString(1);
			string	cur_value = _reader.GetString(2);
			string	cur_desc = _reader.GetString(3);
			int		cur_active = _reader.GetInt32(4);
//			Debug.Log(cur_id.ToString() + "/" + cur_name + "/" + cur_value + "/" + cur_desc + "/" + cur_active.ToString());
		}		
		_reader.Close();_reader = null;_command.Dispose();_command = null;
	}
	
	
}
