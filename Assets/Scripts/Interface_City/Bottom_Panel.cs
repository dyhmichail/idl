﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Bottom_Panel : MonoBehaviour {

	public static bool panel_visible = false;
	public GameObject Panel_Bottom_1;


	public void ShowHidePanel(){
		if (Panel_Bottom_1.GetComponent<LayoutElement>().preferredHeight == 0){
			Panel_Bottom_1.GetComponent<LayoutElement>().preferredHeight = 100;
		} else {
			Panel_Bottom_1.GetComponent<LayoutElement>().preferredHeight = 0;
		}
	}


}
