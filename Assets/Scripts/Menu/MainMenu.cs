﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class MainMenu : MonoBehaviour {

	public static int howManyKings = 12;
	public List<string> KingName = new List<string>();
	public List<string> KingDesc = new List<string>();
	public static List<string> KingImage = new List<string>();

	public static int howManyHeroes = 12;
	public List<string> HeroName = new List<string>();
	public List<string> HeroDesc = new List<string>();
	public static List<string> HeroImage = new List<string>();

	public static int howManyGuilds = 12;
	public List<string> GuildName = new List<string>();
	public List<string> GuildDesc = new List<string>();
	public static List<string> GuildImage = new List<string>();


	public GameObject Canvas_Main;
	public GameObject Panel_Main;

	public GameObject Kings_Panel;
	public Image Image_King_Big;
	public Image Image_King_Choosed;
	public Text Text_King_Title_Big;
	public Text Text_King_Description;
	public GameObject Panel_For_Kings;
	
	public GameObject Heroes_Panel;
	public Image Image_Hero_Big;
	public Image Image_Hero_Choosed;
	public Text Text_Hero_Title_Big;
	public Text Text_Hero_Description;
	public GameObject Panel_For_Heroes;
	
	public GameObject Guilds_Panel;
	public Image Image_Guild_Big;
	public Image Image_Guild_Choosed;
	public Text Text_Guild_Title_Big;
	public Text Text_Guild_Description;
	public GameObject Panel_For_Guilds;

	void Awake(){

		KingName.Add("Регент");
		KingDesc.Add("Борец за свободу\nнедано захвативший власть\nу законного правителя.\nСам из простого народа.");
		KingImage.Add("King1");

		KingName.Add("Крестоносец");
		KingDesc.Add("Все действия совершает\n«Во благо Христа», что делает\nего героем и праведником\nв глазах окружающих.");
		KingImage.Add("King2");

		KingName.Add("Рабовладелец");
		KingDesc.Add("Правитель,\nкоторый практикует рабство.\nВысокая социальная\nответственность.");
		KingImage.Add("King3");

		KingName.Add("Тиран");
		KingDesc.Add("Жестокий правитель в духе\nстрогого диспотизма.\nЖестокая дисциплина приводит\nк общественному порядку.");
		KingImage.Add("King4");

		KingName.Add("Инквизитор");
		KingDesc.Add("Посвятил себя всего служению\nи борьбе со злом,\nотказавшись от мирских благ.\nАскетизм в вещах.");
		KingImage.Add("King5");

		KingName.Add("Храмовник");
		KingDesc.Add("Простой и милостивый человек.\nНе смотря на свою должность\nв первую очередь ставит\nсвои личные интересы.");
		KingImage.Add("King6");
				
		KingName.Add("Пресвистер");
		KingDesc.Add("Основал свою секту\nи свой культ личности.\nПо современному смотрит\nна государства и личности.");
		KingImage.Add("King7");
				
		KingName.Add("Некромант");
		KingDesc.Add("Не вмешивается в мирские дела,\nдавая людям свободу выбора,\nНо проводит тужасные\nэксперименты и пытки.");
		KingImage.Add("King8");

		KingName.Add("Да Винчи");
		KingDesc.Add("Придумывает безопстные\nизобретения.\nЛюбовь к природе и\nокружающему миру.");
		KingImage.Add("King9");
				
		KingName.Add("Джентельмен");
		KingDesc.Add("Сторонник парового двигателя.\nи всего что с этим связано.\nЛично учавствует в\nэкспериментах и экспедициях.");
		KingImage.Add("King10");
				
		KingName.Add("Механик");
		KingDesc.Add("нет описания");
		KingImage.Add("King11");
				
		KingName.Add("Тесла");
		KingDesc.Add("нет описания");
		KingImage.Add("King12");



		HeroName.Add("Варвар");
		HeroDesc.Add("Напоминает берсеркера из\nизвестного аниме «Берсерк»,\nлибо «конан варвар».\nДовольно рас-нный образ.");
		HeroImage.Add("Hero1");
		
		HeroName.Add("Рыцарь");
		HeroDesc.Add("Правильные мысли и поступки.\nХорошие доспехи,\nхороший и без излишеств\nмеч и щит.");
		HeroImage.Add("Hero2");
		
		HeroName.Add("Дуэлянт");
		HeroDesc.Add("В открытом бою толку мало,\nно в определенных\nобстоятельствах действует\nочень эффективно.");
		HeroImage.Add("Hero3");
		
		HeroName.Add("Маг крови");
		HeroDesc.Add("Опасные эксперименты с магией.\nОтравляет своих врагов.\nСпособен излечивать от\nболезней своих соратников.");
		HeroImage.Add("Hero4");
		
		HeroName.Add("Пироман");
		HeroDesc.Add("Молодой офицер способный\nтворить простые заклинания:\nогненный щит,ледяное оружие\nнапоминает наших «гусар»");
		HeroImage.Add("Hero5");
		
		HeroName.Add("Волшебник");
		HeroDesc.Add("Считает что магия это не\nколдовтсво, а тонкая наука.\nЗаклинаня: полиморф,\nтрансформация.");
		HeroImage.Add("Hero6");
		
		HeroName.Add("Экзорцист");
		HeroDesc.Add("Несмотря на корыстный нрав,\nпо возможности помогает\nв неравной борьбе\nс «нечистой силой».");
		HeroImage.Add("Hero7");
		
		HeroName.Add("Падший");
		HeroDesc.Add("Противоположность палладиную.\nСлужит тёмным Богам.\nГотов рисковать жизнью\nи терпеть лишения.");
		HeroImage.Add("Hero8");
		
		HeroName.Add("Колдун");
		HeroDesc.Add("Yет описания");
		HeroImage.Add("Hero9");
		
		HeroName.Add("Страж");
		HeroDesc.Add("Делает свою работу стойко,\nперенося все невзгоды.\nОружие тяжелый арбалет,\nи средний меч.");
		HeroImage.Add("Hero10");
		
		HeroName.Add("Охотник");
		HeroDesc.Add("Юркий и таланливый наемник.\nНе любит рисковать\nи ценит деньги.\nМастер тихих операций");
		HeroImage.Add("Hero11");
		
		HeroName.Add("Снайпер");
		HeroDesc.Add("Классический стрелок\nиз винтовки дальнего боя.\nДевиз: один выстрел - \nодинт труп.");
		HeroImage.Add("Hero12");
		

		
		GuildName.Add("Наемники");
		GuildDesc.Add("нет описания");
		GuildImage.Add("Guild1");
		
		GuildName.Add("Проповедники");
		GuildDesc.Add("нет описания");
		GuildImage.Add("Guild2");
		
		GuildName.Add("Воры");
		GuildDesc.Add("нет описания");
		GuildImage.Add("Guild3");
		
		GuildName.Add("Алхимики");
		GuildDesc.Add("нет описания");
		GuildImage.Add("Guild4");
		
		GuildName.Add("Друиды");
		GuildDesc.Add("нет описания");
		GuildImage.Add("Guild5");
		
		GuildName.Add("Искатели");
		GuildDesc.Add("нет описания");
		GuildImage.Add("Guild6");
		
		GuildName.Add("Чародеи");
		GuildDesc.Add("нет описания");
		GuildImage.Add("Guild7");
		
		GuildName.Add("Меченые");
		GuildDesc.Add("нет описания");
		GuildImage.Add("Guild8");
		
		GuildName.Add("Лекари");
		GuildDesc.Add("нет описания");
		GuildImage.Add("Guild9");
		
		GuildName.Add("Оружейники");
		GuildDesc.Add("нет описания");
		GuildImage.Add("Guild10");
		
		GuildName.Add("Строители");
		GuildDesc.Add("нет описания");
		GuildImage.Add("Guild11");
		
		GuildName.Add("Фермеры");
		GuildDesc.Add("нет описания");
		GuildImage.Add("Guild12");



		generate_Kings();
		generate_Heroes();
		generate_Guilds();

		Kings_Panel.SetActive(false);
		Heroes_Panel.SetActive(false);
		Guilds_Panel.SetActive(false);


	}

	void Update(){

		//Debug.Log(Panel_Main.GetComponent<RectTransform>().rect.width.ToString() + "/" + Screen.width.ToString() + "/" + Screen.height.ToString());
		if (Screen.width > 1000){
			Canvas_Main.GetComponent<CanvasScaler>().matchWidthOrHeight = 1;
		} else {
			Canvas_Main.GetComponent<CanvasScaler>().matchWidthOrHeight = 0;
		}
	}





	public void generate_Guilds(){
		GameObject tempGameObject;
		for (int i = 0; i < howManyGuilds; i++){
			tempGameObject = GameObject.Instantiate(Resources.Load("Prefab/Menu/Panel_Guild", typeof(GameObject)) as GameObject) as GameObject;
			tempGameObject.GetComponent<Guild>().guild_ID = i;
			tempGameObject.GetComponent<Guild>().guild_Name.text = GuildName[i];
			tempGameObject.GetComponent<Guild>().guild_Desc = GuildDesc[i];
			tempGameObject.transform.SetParent(Panel_For_Guilds.transform);
			tempGameObject.transform.localScale = new Vector3(1,1,1);
			tempGameObject.transform.FindChild("Image_Guild").GetComponent<Image>().overrideSprite = Resources.Load<Sprite>("Images/Menu/Guilds/" + GuildImage[i]);
		}
	}
	
	public void Open_Guilds_Panel(){
		Guilds_Panel.SetActive(true);
	}
	
	public void Close_Guilds_Panel(){
		Guilds_Panel.SetActive(false);
	}
	
	public void Guilds_Choosed(){
		Image_Guild_Choosed.GetComponent<Image>().overrideSprite = Resources.Load<Sprite>("Images/Menu/Guilds/" + GuildImage[gm.ChoosedGuild] + "_LARGE");
        gm.Image_Guild_Choosed_Main.overrideSprite = Resources.Load<Sprite>("Images/Menu/Guilds/" + GuildImage[gm.ChoosedGuild] + "_LARGE");

		Guilds_Panel.SetActive(false);
	}
    

	public void generate_Heroes(){
		GameObject tempGameObject;
		for (int i = 0; i < howManyHeroes; i++){
			tempGameObject = GameObject.Instantiate(Resources.Load("Prefab/Menu/Panel_Hero", typeof(GameObject)) as GameObject) as GameObject;
			tempGameObject.GetComponent<Hero>().hero_ID = i;
			tempGameObject.GetComponent<Hero>().hero_Name.text = HeroName[i];
			tempGameObject.GetComponent<Hero>().hero_Desc = HeroDesc[i];
			tempGameObject.transform.SetParent(Panel_For_Heroes.transform);
			tempGameObject.transform.localScale = new Vector3(1,1,1);
			tempGameObject.transform.FindChild("Image_Hero").GetComponent<Image>().overrideSprite = Resources.Load<Sprite>("Images/Menu/Heroes/" + HeroImage[i]);
		}
	}
	
	public void Open_Heroes_Panel(){
		Heroes_Panel.SetActive(true);
	}
	
	public void Close_Heroes_Panel(){
		Heroes_Panel.SetActive(false);
	}
	
	public void Hero_Choosed(){
		Image_Hero_Choosed.GetComponent<Image>().overrideSprite = Resources.Load<Sprite>("Images/Menu/Heroes/" + HeroImage[gm.ChoosedHero] + "_LARGE");
        gm.Image_Hero_Choosed_Main.overrideSprite = Resources.Load<Sprite>("Images/Menu/Heroes/" + HeroImage[gm.ChoosedHero] + "_LARGE");
        Heroes_Panel.SetActive(false);
	}
    

	public void generate_Kings(){
		GameObject tempGameObject;
		for (int i = 0; i < howManyKings; i++){
			tempGameObject = GameObject.Instantiate(Resources.Load("Prefab/Menu/Panel_King", typeof(GameObject)) as GameObject) as GameObject;
			tempGameObject.GetComponent<King>().king_ID = i;
			tempGameObject.GetComponent<King>().king_Name.text = KingName[i];
			tempGameObject.GetComponent<King>().king_Desc = KingDesc[i];
			tempGameObject.transform.SetParent(Panel_For_Kings.transform);
			tempGameObject.transform.localScale = new Vector3(1,1,1);
			tempGameObject.transform.FindChild("Image_King").GetComponent<Image>().overrideSprite = Resources.Load<Sprite>("Images/Menu/Kings/" + KingImage[i]);
		}
	}

	public void Open_Kings_Panel(){
		Kings_Panel.SetActive(true);
	}

	public void Close_Kings_Panel(){
		Kings_Panel.SetActive(false);
	}

	public void King_Choosed(){
		Image_King_Choosed.GetComponent<Image>().overrideSprite = Resources.Load<Sprite>("Images/Menu/Kings/" + KingImage[gm.ChoosedKing] + "_LARGE");
        gm.Image_King_Choosed_Main.overrideSprite = Resources.Load<Sprite>("Images/Menu/Kings/" + KingImage[gm.ChoosedKing] + "_LARGE");
		Kings_Panel.SetActive(false);
	}

}
