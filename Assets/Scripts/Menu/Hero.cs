﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Hero : MonoBehaviour {

	public int hero_ID;
	public Text hero_Name;
	public string hero_Desc;
	public GameObject Canvas_Main;
	
	public void onHeroClick(){
		gm.ChoosedHero = this.hero_ID;
		Canvas_Main = GameObject.Find("Canvas_Menu");
		
		Canvas_Main.GetComponent<MainMenu>().Image_Hero_Big.GetComponent<Image>().overrideSprite = Resources.Load<Sprite>("Images/Menu/Heroes/" + MainMenu.HeroImage[hero_ID] + "_LARGE");
		Canvas_Main.GetComponent<MainMenu>().Text_Hero_Title_Big.text = this.hero_Name.text;
		Canvas_Main.GetComponent<MainMenu>().Text_Hero_Description.text = this.hero_Desc;
	}

}
