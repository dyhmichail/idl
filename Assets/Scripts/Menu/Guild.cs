﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Guild : MonoBehaviour {
	public int guild_ID;
	public Text guild_Name;
	public string guild_Desc;
	public GameObject Canvas_Main;
	
	public void onGuildClick(){
		gm.ChoosedGuild = this.guild_ID;
		Canvas_Main = GameObject.Find("Canvas_Menu");
		
		Canvas_Main.GetComponent<MainMenu>().Image_Guild_Big.GetComponent<Image>().overrideSprite = Resources.Load<Sprite>("Images/Menu/Guilds/" + MainMenu.GuildImage[guild_ID] + "_LARGE");
		Canvas_Main.GetComponent<MainMenu>().Text_Guild_Title_Big.text = this.guild_Name.text;
		Canvas_Main.GetComponent<MainMenu>().Text_Guild_Description.text = this.guild_Desc;
	}
}
