﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class King : MonoBehaviour {
	public int king_ID;
	public Text king_Name;
	public string king_Desc;
	public GameObject Canvas_Main;

	public void onKingClick(){
		gm.ChoosedKing = this.king_ID;
		Canvas_Main = GameObject.Find("Canvas_Menu");
		Canvas_Main.GetComponent<MainMenu>().Image_King_Big.GetComponent<Image>().overrideSprite = Resources.Load<Sprite>("Images/Menu/Kings/" + MainMenu.KingImage[king_ID] + "_LARGE");
		Canvas_Main.GetComponent<MainMenu>().Text_King_Title_Big.text = this.king_Name.text;
		Canvas_Main.GetComponent<MainMenu>().Text_King_Description.text = this.king_Desc;
	}

}
