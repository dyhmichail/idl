﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Region {

	public static int current_ID = 1;
	public int region_ID;
	public string region_Name;
	public bool region_Real = false;
	public int timeToDelivery;
	public GameObject GameObject_City;
	public GameObject region_PeoplesDock;

	public List<People> Peoples = new List<People>();
	public List<Building> Buildings = new List<Building>();
	public List<GameObject> Colliders_4_Build = new List<GameObject>();

	public Dictionary<GoodType, int> region_Needed = new Dictionary<GoodType, int>();
	public Dictionary<GoodType, int> warehouse_Reserved = new Dictionary<GoodType, int>();

	public Region(
		string _region_Name,
		int _timeToDelivery,
		bool _region_Real
		)
	{
		region_ID = Region.current_ID++;
		region_Name = _region_Name;
		timeToDelivery = _timeToDelivery;
		region_Real = _region_Real;

		foreach (KeyValuePair<string, GoodType> pairGood in gm.typeOfGood){
			region_Needed.Add(pairGood.Value, 0);
		}

		foreach(KeyValuePair<string, GoodType> pair in gm.typeOfGood){
			warehouse_Reserved.Add(pair.Value, 0);
		}

		if ((region_Real == true) || (region_Name == "Outside")){
			GameObject_City = GameObject.Find("GameObject_" + region_Name);
			region_PeoplesDock = this.GameObject_City.transform.FindChild("Canvas_Region_Info").FindChild("Panel_Peoples_Dock").gameObject;
			GameObject[] result = GameObject.FindGameObjectsWithTag("Collider_4_Building");
			for(int i = 0; i < result.Length; i++){
				if ( result[i].transform.parent.name == GameObject_City.name){
					Colliders_4_Build.Add (result[i]);
				}
			}
		}
	}

	public static void ResetAllRegionsNeeded(){
		foreach (KeyValuePair<string, Region> pairRegion in gm.Regions){
			foreach (KeyValuePair<string, GoodType> pairGood in gm.typeOfGood){
				pairRegion.Value.region_Needed[pairGood.Value] = 0;
			}
		}
	}

    public static int RegionProduceProtection(Region currentRegion)
    {
        int tempHave = 0;
        foreach (Building currentBuilding in currentRegion.Buildings)
        {
            foreach (GoodType currentGood in currentBuilding.building_Type.building_produce.Keys)
            {
                if (currentGood.good_Name == "protection")
                {
                    tempHave += currentBuilding.slotsFilled*currentBuilding.building_Type.building_produce[gm.typeOfGood["protection"]];
                }
            }
        }
        return tempHave;
    }
}
