﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Warehouse2 {

	public int warehouse_ID;
	public string warehouse_Name;
	
	public Dictionary<GoodType, int> warehouse_Reserved = new Dictionary<GoodType, int>();
	public Region warehouse_RegionOwner;
	
	
	public Warehouse2(Region _warehouse_RegionOwner){
		warehouse_RegionOwner = _warehouse_RegionOwner;
		warehouse_ID = warehouse_RegionOwner.region_ID;
		warehouse_Name = "W" + warehouse_RegionOwner.region_Name;
		
		foreach(KeyValuePair<string, GoodType> pair in gm.typeOfGood){
			warehouse_Reserved.Add(pair.Value, 0);
		}
	}
	
/*	
	public static void AddRemoveGoodToWarehouse(Warehouse warehouseWhere, typesOfGoods goodType, int quantity, bool addOrRemove){
		
		int temp = warehouseWhere.warehouseReserved[goodType];
		warehouseWhere.warehouseReserved[goodType] = addOrRemove ? temp + quantity : temp - quantity;
	}
*/	
	

}
