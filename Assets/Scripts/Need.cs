﻿using UnityEngine;
using System.Collections;

public class Need {
	public static int current_ID = 1;
	public int need_ID;
	public string need_Name;
	public GoodType need_Type;
    public BuildingType need_BuildingConstructType;
    public Building need_BuildingConstruct;
    public int need_BuildingConstructRestTime = 0;
	public bool need_ToDelete = false;

	public Need(BuildingType currentBuilding){
		need_ID = current_ID++;
		need_Name = "Need_" + currentBuilding.building_Name;
		need_BuildingConstruct = null;
		need_BuildingConstructRestTime = 0;
	}

	public static Need Add_Need(BuildingType currentBuildingType){
		Need tempNeedClass = null;
		bool needExists = false;
		foreach(Need need in gm.Needs){
	        if (need.need_BuildingConstructType == currentBuildingType)
	        {
	            needExists = true;
		    }
		}
	    if (needExists == false)
	    {
            tempNeedClass = new Need(currentBuildingType);
            tempNeedClass.need_BuildingConstructType = currentBuildingType;
            gm.Needs.Add(tempNeedClass);
	        return tempNeedClass;
	    }
	    return null;
	}
}
