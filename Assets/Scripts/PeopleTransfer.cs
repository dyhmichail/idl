﻿using UnityEngine;
using System.Collections;

public class PeopleTransfer {

	public static int current_ID = 1;
	public int transfer_ID;
	public Region fromTransfer;
	public Region whomTransfer;
	public int timeRestTransfer;
	public People peopleTransferred;
	public bool toDelete;
	public PeopleTransferType transferReason;

	public PeopleTransfer(Region _fromTransfer, Region _whomTransfer, int _timeRestTransfer, People _peopleTransferred, PeopleTransferType _transferReason, bool _toDelete){
		transfer_ID = current_ID++;
		fromTransfer = _fromTransfer;
		whomTransfer = _whomTransfer;
		timeRestTransfer = _timeRestTransfer;
		peopleTransferred = _peopleTransferred;
		toDelete = _toDelete;
		transferReason = _transferReason;
	}

	public static PeopleTransfer TransferCitizenFromTo(Region regionMoveTo, People citizen, PeopleTransferType transferReason){
		int tempTime = 0;
		PeopleTransfer tempTransfer;
		
		if (regionMoveTo == gm.Regions["Castle"]){
			tempTime = citizen.people_RegionWherePlaced.timeToDelivery + regionMoveTo.timeToDelivery;
		} else {
			tempTime = regionMoveTo.timeToDelivery;
		}
		tempTransfer = new PeopleTransfer(citizen.people_RegionWherePlaced, regionMoveTo, tempTime, citizen, transferReason, false);
		gm.PeoplesTransferList.Add(tempTransfer);
		citizen.people_RegionWherePlaced.Peoples.Remove(citizen);
		citizen.people_RegionWherePlaced = null;
		citizen.people_GameObject.transform.SetParent(null);
		return tempTransfer;
	}

	public static void ShowTransfersOnReason(PeopleTransferType reason, Stat stat){
		foreach(PeopleTransfer tempTransfer in gm.PeoplesTransferList){
			if(tempTransfer.transferReason == reason){
				stat.statsBody += tempTransfer.transfer_ID.ToString("000") + " | " +
					"" +  tempTransfer.transferReason.people_transfer_Name + " | " +
						tempTransfer.fromTransfer.region_Name.ToString() + " > > " +
						tempTransfer.whomTransfer.region_Name.ToString() + " | " +
						"ID - " +   tempTransfer.peopleTransferred.people_ID.ToString("000") + " | " +
						"врем. ост - " + tempTransfer.timeRestTransfer.ToString() + " | " + 
						"к уд. - " + tempTransfer.toDelete.ToString() + "\n";
			}
		}
	}
}


public class PeopleTransferType {
	public int people_transfer_ID;
	public string people_transfer_Name;

	public PeopleTransferType(int    _people_transfer_ID,
	                          string _people_transfer_Name)
	{
		people_transfer_ID   = _people_transfer_ID;
		people_transfer_Name = _people_transfer_Name;
	}

}