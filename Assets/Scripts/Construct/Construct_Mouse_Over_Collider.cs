﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Construct_Mouse_Over_Collider : MonoBehaviour {

	public bool startConstruct = false;
	public bool constructInProgress = false;
	public int time_to_build;
	public int buildCurrent = 0;
	public Color spriteColor;
	public GameObject ProgressBar;
	public GameObject ProgressBar_Under;
	public Text Text_Need;
	public bool finished = false;

	
	void OnMouseOver(){
		if(startConstruct == false){
			Construct_Main.BuildToConstructDocked = true;
			Construct_Main.Construct_Cursor.transform.position = transform.position;
			Construct_Main.currentCollider = gameObject;
		}
	}

	void OnMouseEnter(){
		if(startConstruct == false){
			Construct_Main.BuildToConstructDocked = true;
			Construct_Main.Button_Start_Construct.SetActive(true);
		}
	}

	void OnMouseExit(){
		if(startConstruct == false){
			Construct_Main.BuildToConstructDocked = false;
			Construct_Main.Button_Start_Construct.SetActive(false);
		}
	}

}
