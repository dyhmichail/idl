﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Construct_Item : MonoBehaviour {

	public event gm.ShowHideInfoPanel onMouseOnButton;

	void Start(){
		onMouseOnButton += new gm.ShowHideInfoPanel(Camera.main.GetComponent<gm>().ShowHide_PanelBottomInfo);
	}

	public void OnItemClick(){

		if (Building.Get_Free_Collider_For_Construct(gm.Regions[gm.current_Region]) != null){
			if (gm.All_Mats_To_Build_Present(gm.typeOfBuilding[this.GetComponent<Image>().sprite.name], true) == true){
				Construct_Main.Construct_Cursor.SetActive(true);
				Construct_Main.Button_Start_Construct.SetActive(false);
				foreach (Transform child in gm.Regions[gm.current_Region].GameObject_City.transform){
					if ((child.GetComponent<Construct_Mouse_Over_Collider>() != null) && (child.GetComponent<Construct_Mouse_Over_Collider>().startConstruct == false)){
						child.gameObject.SetActive(true);
					}
				}
				Construct_Main.Construct_Cursor.GetComponent<SpriteRenderer>().sprite = this.GetComponent<Image>().sprite;
			    Construct_Main.CurrentBuildingTypeToConstruct = gm.typeOfBuilding[this.GetComponent<Image>().sprite.name];
			} else {
				Debug.Log("Not enough materials!");
			}
		} else {
			Debug.Log("No one free collider!");
		}

	}

	public void OnMouseEnter(){
		onMouseOnButton(true, gm.typeOfBuilding[this.GetComponent<Image>().sprite.name]);
	}

	public void OnMouseExit(){
		onMouseOnButton(false, gm.typeOfBuilding[this.GetComponent<Image>().sprite.name]);
	}



}
