﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Construct_Main : MonoBehaviour {

	public GameObject Panel_To_Construct_Content;
	public static GameObject Construct_Cursor;
	public static GameObject Button_Start_Construct;

	public static GameObject currentCollider;
	public static bool BuildToConstructDocked = false;
	public Vector3 currentMousePosition;
    public static BuildingType CurrentBuildingTypeToConstruct;

	void Awake(){
		Button_Start_Construct = GameObject.Find("Button_Start_Construct");
		Button_Start_Construct.SetActive(true);
		Construct_Cursor = GameObject.Find("Construct_Cursor");
		Construct_Cursor.SetActive(false);
		Generate_Panel_To_Construct_Content();
		BuildToConstructDocked = false;
	}

	void Update(){
        
		if (Input.GetMouseButtonDown(1)){
			Construct_Cursor.SetActive(false);
			Button_Start_Construct.SetActive(false);
			foreach (Transform child in gm.Regions[gm.current_Region].GameObject_City.transform){
				if ((child.GetComponent<Construct_Mouse_Over_Collider>() != null) && (child.GetComponent<Construct_Mouse_Over_Collider>().startConstruct == false)){
					child.gameObject.SetActive(false);
				}
			}
		}

		if (BuildToConstructDocked == false){
			currentMousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Construct_Cursor.transform.position = new Vector3(currentMousePosition.x, currentMousePosition.y, 0);
		}
	}

	public void Generate_Panel_To_Construct_Content(){
		foreach (KeyValuePair<string, BuildingType> pair in gm.typeOfBuilding){
			if (pair.Value.building_Real){
				GameObject new_Building = GameObject.Instantiate(Resources.Load("Prefab/Image_Building_To_Construct_", typeof(GameObject))) as GameObject;
				new_Building.GetComponent<Image>().sprite = Resources.Load("Images/Buildings/" + pair.Value.building_Name, typeof(Sprite)) as Sprite;
				new_Building.GetComponentInChildren<Text>().text = pair.Value.building_Name;
				new_Building.transform.SetParent(Panel_To_Construct_Content.transform);
				new_Building.transform.localScale = new Vector3(1,1,1);
			}
		}
	}

	public void BuldingChoosedPlaceClickOK(){

		gm.Remove_Mats_To_Build(gm.typeOfBuilding[Construct_Cursor.GetComponent<SpriteRenderer>().sprite.name], true);
		gm.Refresh_Warehouse_Panel(false);

		Construct_Cursor.SetActive(false);
		Button_Start_Construct.SetActive(true);
		currentCollider.GetComponent<Construct_Mouse_Over_Collider>().startConstruct = true;

		foreach (Transform child in gm.Regions[gm.current_Region].GameObject_City.transform){
			if ((child.GetComponent<Construct_Mouse_Over_Collider>() != null) && (child.GetComponent<Construct_Mouse_Over_Collider>().startConstruct == false)){
				child.gameObject.SetActive(false);
			}
		}
        Debug.Log("Manual Add");

        Building tempBuilding = null;
        tempBuilding = Building.Add_Building(gm.Regions[gm.current_Region], CurrentBuildingTypeToConstruct, true, currentCollider);
        foreach (KeyValuePair<GoodType, int> pairGoodNeed in tempBuilding.building_Type.need_4_Build)
        {
            gm.Regions["Player"].warehouse_Reserved[pairGoodNeed.Key] -= pairGoodNeed.Value;
            tempBuilding.whatNeedToConstructStored[pairGoodNeed.Key]  += pairGoodNeed.Value;
        }


	}
}
