﻿using UnityEngine;
using System.Collections;

public class Stat : MonoBehaviour {

	public static int current_ID = 1;
	public int stat_ID;
	public string stat_Name;
	public string stat_Title;
    public GameObject stat_GameObject;
    public GameObject stat_GameObject_Enable;

	public string statsBody;
	
	
	public Stat(string _stat_Name, GameObject _stat_GameObject, GameObject _stat_GameObject_Enable, string _stat_Title){
		stat_ID = current_ID++;
		stat_Name = _stat_Name;
		stat_Title = _stat_Title;
		stat_GameObject = _stat_GameObject;
        stat_GameObject_Enable =_stat_GameObject_Enable;
		statsBody = "";
	}

}
