﻿using System.CodeDom.Compiler;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class gm : MonoBehaviour {

	public static Dictionary<string, Region>             Regions              = new Dictionary<string, Region>();
	public static Dictionary<string, BuildingType>       typeOfBuilding       = new Dictionary<string, BuildingType>();
	public static Dictionary<string, ProductType>        typeOfProduct        = new Dictionary<string, ProductType>();
	public static Dictionary<string, GoodType>           typeOfGood           = new Dictionary<string, GoodType>();
	public static Dictionary<string, SlotType>			 typeOfSlot           = new Dictionary<string, SlotType>();
	public static Dictionary<string, PeopleTransferType> typeOfPeopleTransfer = new Dictionary<string, PeopleTransferType>();

	public static List<Slot> Slots = new List<Slot>();
	public static List<Need> Needs = new List<Need>();
    public static List<Stat> Stats = new List<Stat>();
	public static List<PeopleTransfer> PeoplesTransferList = new List<PeopleTransfer>();
	public static List<DeliveryTask> deliveryTaskList = new List<DeliveryTask>();
	public static List<People> PeoplesReadyToWar = new List<People>();
    public static List<Building> ConstructBuildings = new List<Building>();
    public static List<Taxe> Taxes = new List<Taxe>();
    
    public static List<GameObject> Bottom_Panels = new List<GameObject>();
    public static List<GameObject> Bottom_Buttons = new List<GameObject>();


    public static int ChoosedGuild;
    public static int ChoosedHero;
    public static int ChoosedKing;

	// Loaded from |start_params| Table
	public static int init_population;
	public static int max_population; 
	public static int add_people_per_food_quantity; 
	public static int min_goods_to_transfer;
	public static int percent_extra_goods_to_transfer;
	public static int eat_per_tour;
	public static int use_goods_per_tour;
	public static int need_protection_per_tour;
	public static int teac_duration;    
	public static int food_limit;
	public static int food_limit_now;        
	public static int food_limit_random_min; 
	public static int food_limit_random_max; 
	public static int protect_limit;         
	public static int protect_limit_now;     
	public static int protect_limit_random_min;
	public static int protect_limit_random_max;
	public static int goods_limit;           
	public static int goods_limit_now;       
	public static int goods_limit_random_min;
	public static int goods_limit_random_max;
    public static int min_army;
    public static int max_army;
    public static int percent_random_people_moving;
    public static int default_tax_rate;

	public static string current_Region = "Castle";
	public GameObject Panel_Building_To_Construct;
	public static GameObject Panel_Slots;
	public static Text Text_Good_Name, Text_Good_Player, Text_Good_Region;
	public static GameObject Panel_Bottom_Building_Info;
	public static GameObject Button_Fight;
	public static Text Text_Materials, Text_Produce;
	public static Text Stats_Window;
	public static GameObject Panel_Stats;
	public static GameObject PanelLeavedPeoples;
    public static GameObject Fight_Main, Background_Main, Menu_Main;
    public static Color baseColor = new Color(255,200,0,1);

    public static GameObject Panel_Content_Items_Regions;
    public static GameObject Panel_Content_Items_Products;
    
    public static Text Text_Percentage_Taxes;
    public static Slider Slider_Percentage_Taxes;
    public static GameObject Panel_Bottom_Taxes_Info;
    public static Text Text_Regions_Left;
    public static bool reset_Taxe;

    public static Image Image_King_Choosed_Main;
    public static Image Image_Hero_Choosed_Main;
    public static Image Image_Guild_Choosed_Main;
    public static Text Text_Population;

    public static GameObject CurrentPeoplePersonal;

	public delegate void ShowHideInfoPanel(bool showPanel, BuildingType currentBuilding);
//	public delegate void ShowHideFightButton(bool showButton);

	void Awake(){
		Debug.Log(Application.persistentDataPath + "/");
		//Fill Static variables
        Fight_Main                  = GameObject.Find("Fight_Main");
        Background_Main             = GameObject.Find("Background_Main");
        Menu_Main                   = GameObject.Find("Menu_Main");
		Panel_Bottom_Building_Info	= GameObject.Find("Panel_Bottom_Building_Info");
		Button_Fight                = GameObject.Find("Button_Fight");
		Text_Materials				= GameObject.Find("Text_Materials").GetComponent<Text>();
		Text_Produce				= GameObject.Find("Text_Produce").GetComponent<Text>();
		Text_Good_Name				= GameObject.Find("Text_Good_Name").GetComponent<Text>();
		Text_Good_Player			= GameObject.Find("Text_Good_Player").GetComponent<Text>();
		Text_Good_Region			= GameObject.Find("Text_Good_Region").GetComponent<Text>();
		Panel_Slots					= GameObject.Find("Panel_Slots");
		Stats_Window				= GameObject.Find("Stats_Window").GetComponent<Text>();
		Panel_Stats					= GameObject.Find("Panel_Stats");
		PanelLeavedPeoples          = GameObject.Find("PanelLeavedPeoples");

        Panel_Content_Items_Regions  = GameObject.Find("Panel_Content_Items_Regions");
        Panel_Content_Items_Products = GameObject.Find("Panel_Content_Items_Products");

        Text_Percentage_Taxes   = GameObject.Find("Text_Percentage_Taxes").GetComponent<Text>();
        Slider_Percentage_Taxes = GameObject.Find("Slider_Percentage_Taxes").GetComponent<Slider>();
        Panel_Bottom_Taxes_Info = GameObject.Find("Panel_Bottom_Taxes_Info");
        Text_Regions_Left       = GameObject.Find("Text_Regions_Left").GetComponent<Text>();

        Image_King_Choosed_Main  = GameObject.Find("Image_King_Choosed_Main").GetComponent<Image>(); ;
        Image_Hero_Choosed_Main  = GameObject.Find("Image_Hero_Choosed_Main").GetComponent<Image>(); ;
        Image_Guild_Choosed_Main = GameObject.Find("Image_Guild_Choosed_Main").GetComponent<Image>(); ;
        Text_Population = GameObject.Find("Text_Population").GetComponent<Text>();


        Bottom_Panels.Add(GameObject.Find("Panel_Buildings_To_Construct"));
        Bottom_Panels.Add(GameObject.Find("Panel_Taxes"));
        Bottom_Panels.Add(GameObject.Find("Panel_Bottom_03"));
        Bottom_Panels.Add(GameObject.Find("Panel_Bottom_04"));

        Bottom_Buttons.Add(GameObject.Find("Button_Bottom_01"));
        Bottom_Buttons.Add(GameObject.Find("Button_Bottom_02"));
        Bottom_Buttons.Add(GameObject.Find("Button_Bottom_03"));
        Bottom_Buttons.Add(GameObject.Find("Button_Bottom_04"));

		Button_Fight.SetActive(false);
		Panel_Bottom_Building_Info.SetActive(false);


		// Load Dictionary's from SQL
		Load_Params_Info();
		Load_Good_Info();
		Load_Region_Info();
		Change_Region_GameObject("Castle");
        Load_Slot_Info();
		Load_Building_Info();
		Load_Product_Info();
		Load_PeopleTransfer_Info();

		//Random Values to Warehoses
		Refresh_Warehouse_Panel(true);
		Add_Start_Values();
	    Load_Bottom_Toggles();
        Refresh_Population_Panel();

        Fight_Main.transform.position = new Vector3(0, 20, 0);
        Background_Main.transform.position = new Vector3(15, 0, 0);
        Menu_Main.transform.position = new Vector3(0, 0, 0);
        reset_Taxe = false;

	}

	public void Add_Start_Values()
	{
	    People tempPeople;
		for (int i = 1; i < init_population + 1; i++){
			tempPeople = People.Add_People(Regions["Castle"], false);
            
		    if (i < 6)
		    {
		        gm.PeoplesReadyToWar.Add(tempPeople);
		        tempPeople.people_InArmyNow = true;
		    }
            
		}
	    nt.CheckCompleteArmy();
        RearrangeBottomButtonsAndPanels(0);

		Slot.Add_Slot(typeOfSlot["Farmer"], true);
		Slot.Add_Slot(typeOfSlot["Farmer"], true);
		Slot.Add_Slot(typeOfSlot["Lumberjack"], true);
		Slot.Add_Slot(typeOfSlot["Worker"], true);
		Slot.Add_Slot(typeOfSlot["Worker"], true);
		Slot.Add_Slot(typeOfSlot["Worker"], true);
		Slot.Add_Slot(typeOfSlot["Soldier"], true);
		Slot.Add_Slot(typeOfSlot["Soldier"], true);

		Building.Add_Building(Regions["Castle"], gm.typeOfBuilding["Patrol"],   false, null);
        Building.Add_Building(Regions["Farm"], gm.typeOfBuilding["Patrol"],     false, null);
        Building.Add_Building(Regions["Mine"], gm.typeOfBuilding["Patrol"],     false, null);
        Building.Add_Building(Regions["Forest"], gm.typeOfBuilding["Patrol"],   false, null);
//        Building.Add_Building(Regions["Castle"], gm.typeOfBuilding["Castle"],   false, null);
        Building.Add_Building(Regions["Farm"], gm.typeOfBuilding["Farm"],       false, null);
        Building.Add_Building(Regions["Castle"], gm.typeOfBuilding["Workshop"], false, null);
        Building.Add_Building(Regions["Forest"], gm.typeOfBuilding["Sawmill"],  false, null);

        //Building.Add_Building(Regions["Castle"], gm.typeOfBuilding["Workshop"],  true, null);


        Stats.Add(new Stat("CollectResourcesFromBuildings", GameObject.Find("Toggle_01"), GameObject.Find("Toggle_01_1"), " -- Отчёт по сбору произведённых ресурсов со зданий --------------------------\n"));
        Stats.Add(new Stat("PrepareToTransferResources",    GameObject.Find("Toggle_02"), GameObject.Find("Toggle_02_1"), " -- Отчёт о подготовке накладных для перемещения товаров между регионами -----\n"));
        Stats.Add(new Stat("PeoplesFormOutside",            GameObject.Find("Toggle_03"), GameObject.Find("Toggle_03_1"), " -- Отчёт о приходе людей на излишки еды -----\n"));
        Stats.Add(new Stat("RandomPeopleMovingNearRegions", GameObject.Find("Toggle_04"), GameObject.Find("Toggle_04_1"), " -- Отчёт о случайных перемещениях людей между регионами -----\n"));
        Stats.Add(new Stat("FreeSlotsDistribution",         GameObject.Find("Toggle_05"), GameObject.Find("Toggle_05_1"), " -- Распределение свободных слотов по зданиям -----\n"));
        Stats.Add(new Stat("FindPeopleForSlotsInBuildings", GameObject.Find("Toggle_06"), GameObject.Find("Toggle_06_1"), " -- Поиск людей для слотов в зданиях -----\n"));
        Stats.Add(new Stat("ShowAllNeeds",                  GameObject.Find("Toggle_07"), GameObject.Find("Toggle_07_1"), " -- Просмотр всех необходимостей -----\n"));
        Stats.Add(new Stat("ConstructBuildings",            GameObject.Find("Toggle_08"), GameObject.Find("Toggle_08_1"), " -- Строительство зданий -----\n"));
        Stats.Add(new Stat("CollectFoodRequests",           GameObject.Find("Toggle_09"), GameObject.Find("Toggle_09_1"), " -- Сбор запросов на еду и проставление уровня -----\n"));
        Stats.Add(new Stat("CollectGoodRequests",           GameObject.Find("Toggle_10"), GameObject.Find("Toggle_10_1"), " -- Сбор запросов на блага и проставление уровня -----\n"));
        Stats.Add(new Stat("CollectProtectRequests",        GameObject.Find("Toggle_11"), GameObject.Find("Toggle_11_1"), " -- Сбор запросов на защиту и проставление уровня -----\n"));

		for (int i = 0; i < Stats.Count; i++){
            Stats[i].stat_GameObject.GetComponent<Toggle>().isOn = PlayerPrefs.GetInt(Stats[i].stat_GameObject.name) == 1 ? true : false;
            Stats[i].stat_GameObject_Enable.GetComponent<Toggle>().isOn = PlayerPrefs.GetInt(Stats[i].stat_GameObject_Enable.name) == 1 ? true : false;
        }

        OverPeople.PersonalCanvas = GameObject.Find("Canvas_Personal");
        //OverPeople.PersonalCanvas.SetActive(false);
	}
	
	void Load_Params_Info(){
		Sql_Init._command = Sql_Init._connection.CreateCommand();
		Sql_Init.sql = "select _id, name, value from start_params where real = 1";
		Sql_Init._command.CommandText = Sql_Init.sql;Sql_Init._reader = Sql_Init._command.ExecuteReader();
		while (Sql_Init._reader.Read()){
			int		current_id	 = Sql_Init._reader.GetInt32(0);
			string	current_name = Sql_Init._reader.GetString(1);
			int 	current_value = Sql_Init._reader.GetInt32(2);

			switch(current_id){
				case 1:	init_population = current_value; break;
				case 2:	max_population = current_value;	break;
				case 3:	add_people_per_food_quantity = current_value; break;
				case 4:	min_goods_to_transfer = current_value; break;
				case 5:	percent_extra_goods_to_transfer = current_value; break;
				case 6:	eat_per_tour = current_value; break;
				case 7:	use_goods_per_tour = current_value; break;
				case 8:	need_protection_per_tour = current_value; break;
				case 9:	teac_duration = current_value; break;
			
				case 10: food_limit = current_value; break;
				case 11: food_limit_now = current_value; break;
				case 12: food_limit_random_min = current_value; break;
				case 13: food_limit_random_max = current_value; break;
				case 14: protect_limit = current_value; break;
				case 15: protect_limit_now = current_value; break;
				case 16: protect_limit_random_min = current_value; break;
				case 17: protect_limit_random_max = current_value; break;
				case 18: goods_limit = current_value; break;
				case 19: goods_limit_now = current_value; break;
				case 20: goods_limit_random_min = current_value; break;
				case 21: goods_limit_random_max = current_value; break;

                case 22: min_army = current_value; break;
                case 23: max_army = current_value; break;
                case 24: percent_random_people_moving = current_value; break;
                case 25: default_tax_rate = current_value; break;
                   
			}
		}		
		Sql_Init._reader.Close();Sql_Init._reader = null;Sql_Init._command.Dispose();Sql_Init._command = null;
	}

	void Load_Region_Info(){
		Sql_Init._command = Sql_Init._connection.CreateCommand();
		Sql_Init.sql = "select _id, name, time_to_delivery, real from regions";
		Sql_Init._command.CommandText = Sql_Init.sql;Sql_Init._reader = Sql_Init._command.ExecuteReader();
		while (Sql_Init._reader.Read()){
			int		current_id	 = Sql_Init._reader.GetInt32(0);
			string	current_name = Sql_Init._reader.GetString(1);
			int 	current_time = Sql_Init._reader.GetInt32(2);
			int		current_real = Sql_Init._reader.GetInt32(3);
			Regions.Add(current_name, new Region(current_name, current_time,  current_real == 0 ? false : true));
		}		
		Sql_Init._reader.Close();Sql_Init._reader = null;Sql_Init._command.Dispose();Sql_Init._command = null;
	}

	public static void Change_Region_GameObject(string visibleRegion){
		foreach(KeyValuePair<string, Region> pairRegion in Regions){
			if (pairRegion.Value.GameObject_City != null){
				if(pairRegion.Value.GameObject_City.name == "GameObject_" + visibleRegion){
					pairRegion.Value.GameObject_City.transform.position = new Vector3(0,0,0);
				} else {
					pairRegion.Value.GameObject_City.transform.position = new Vector3(0, pairRegion.Value.region_ID * -10,0);
				}
			}
		}
	}

	void Load_Building_Info(){
		Sql_Init._command = Sql_Init._connection.CreateCommand();
        Sql_Init.sql = "select _id, name, real, seats_quantity, time_to_build, id_slots, builders_need from buildings";
		Sql_Init._command.CommandText = Sql_Init.sql;Sql_Init._reader = Sql_Init._command.ExecuteReader();
		while (Sql_Init._reader.Read()){
			int		current_id	       = Sql_Init._reader.GetInt32(0);
			string	current_name       = Sql_Init._reader.GetString(1);
			int		current_real       = Sql_Init._reader.GetInt32(2);
			int 	cur_seats_quantity = Sql_Init._reader.GetInt32(3);
			int 	cur_time_to_build  = Sql_Init._reader.GetInt32(4);
            int     cur_slot_id        = Sql_Init._reader.GetInt32(5);
            int     cur_builders_need  = Sql_Init._reader.GetInt32(6);
            
			typeOfBuilding.Add(current_name, new BuildingType(current_id, current_name, current_real == 0 ? false : true, cur_seats_quantity, cur_time_to_build, cur_slot_id, cur_builders_need));
		}		
		Sql_Init._reader.Close();Sql_Init._reader = null;Sql_Init._command.Dispose();Sql_Init._command = null;

		foreach(KeyValuePair<string, BuildingType> pair in typeOfBuilding){
			Load_Need_4_Build(pair.Value.building_ID, pair.Value.building_Name);
			Load_Building_Produce(pair.Value.building_ID, pair.Value.building_Name);
		}
	}

	void Load_Need_4_Build(int building_ID, string building_Type){
		Sql_Init._command = Sql_Init._connection.CreateCommand();
		Sql_Init.sql = System.String.Format("select goods.name, need_4_build.quantity from goods, need_4_build where need_4_build.id_buildings = {0} and need_4_build.id_goods = goods._id",
		                                    building_ID.ToString());

		Sql_Init._command.CommandText = Sql_Init.sql;Sql_Init._reader = Sql_Init._command.ExecuteReader();
		while (Sql_Init._reader.Read()){
			string	current_good     = Sql_Init._reader.GetString(0);
			int		current_quantity = Sql_Init._reader.GetInt32(1);
			typeOfBuilding[building_Type].need_4_Build.Add(typeOfGood[current_good], current_quantity);
		}		
		Sql_Init._reader.Close();Sql_Init._reader = null;Sql_Init._command.Dispose();Sql_Init._command = null;
	}

	void Load_Building_Produce(int building_ID, string building_Type){
		Sql_Init._command = Sql_Init._connection.CreateCommand();
		Sql_Init.sql = System.String.Format("select goods.name, building_produce.per_seat from goods, building_produce where building_produce.id_buildings = {0} and building_produce.id_goods = goods._id",
		                                    building_ID.ToString());
		
		Sql_Init._command.CommandText = Sql_Init.sql;Sql_Init._reader = Sql_Init._command.ExecuteReader();
		while (Sql_Init._reader.Read()){
			string	current_good     = Sql_Init._reader.GetString(0);
			int		current_per_seat = Sql_Init._reader.GetInt32(1);
			typeOfBuilding[building_Type].building_produce.Add(typeOfGood[current_good], current_per_seat);
		}		
		Sql_Init._reader.Close();Sql_Init._reader = null;Sql_Init._command.Dispose();Sql_Init._command = null;
	}

    private void Load_Product_Info()
    {
        Sql_Init._command = Sql_Init._connection.CreateCommand();
        Sql_Init.sql = "select _id, name, real from products";
        Sql_Init._command.CommandText = Sql_Init.sql;
        Sql_Init._reader = Sql_Init._command.ExecuteReader();
        while (Sql_Init._reader.Read())
        {
            int current_id = Sql_Init._reader.GetInt32(0);
            string current_name = Sql_Init._reader.GetString(1);
            int current_real = Sql_Init._reader.GetInt32(2);
            typeOfProduct.Add(current_name, new ProductType(current_id, current_name, current_real == 0 ? false : true));
        }
        Sql_Init._reader.Close();
        Sql_Init._reader = null;
        Sql_Init._command.Dispose();
        Sql_Init._command = null;

        foreach (KeyValuePair<string, ProductType> pairProduct in typeOfProduct)
        {
            if (pairProduct.Value.product_Real == true)
            {
                GameObject tempGameObject = Resources.Load("Prefab/BottomPanel/Text_Product_", typeof (GameObject)) as GameObject;
                GameObject new_Info_Panel = GameObject.Instantiate(tempGameObject) as GameObject;
                new_Info_Panel.name = "Text_Product_" + pairProduct.Value.product_Name;
                new_Info_Panel.GetComponent<Text>().text = pairProduct.Value.product_Name + "\n";
                new_Info_Panel.transform.SetParent(Panel_Bottom_Taxes_Info.transform);
                new_Info_Panel.transform.localScale = new Vector3(1, 1, 1);
            }
        }

    }

	void Load_Good_Info(){
		Sql_Init._command = Sql_Init._connection.CreateCommand();
		Sql_Init.sql = "select _id, name, real, id_products from goods";
		Sql_Init._command.CommandText = Sql_Init.sql;Sql_Init._reader = Sql_Init._command.ExecuteReader();
		while (Sql_Init._reader.Read()){
			int		current_id	       = Sql_Init._reader.GetInt32(0);
			string	current_name       = Sql_Init._reader.GetString(1);
			int		current_real       = Sql_Init._reader.GetInt32(2);
			int 	cur_id_products    = Sql_Init._reader.GetInt32(3);
			typeOfGood.Add(current_name, new GoodType(current_id, current_name, current_real == 0 ? false : true, cur_id_products));
		}		
		Sql_Init._reader.Close();Sql_Init._reader = null;Sql_Init._command.Dispose();Sql_Init._command = null;

		foreach(KeyValuePair<string, GoodType> pair in typeOfGood){
			Load_Need_4_Produce(pair.Value.good_ID, pair.Value.good_Name);
		}
	}

	void Load_Need_4_Produce(int good_ID, string good_Type){
		Sql_Init._command = Sql_Init._connection.CreateCommand();

		Sql_Init.sql = System.String.Format("select goods.name, good_need.quantity from goods, good_need where good_need.id_good_produce = {0} and good_need.id_good_need = goods._id",
		                                    good_ID.ToString());
		Sql_Init._command.CommandText = Sql_Init.sql;Sql_Init._reader = Sql_Init._command.ExecuteReader();
		while (Sql_Init._reader.Read()){
			string	current_good     = Sql_Init._reader.GetString(0);
			int		current_quantity = Sql_Init._reader.GetInt32(1);
			typeOfGood[good_Type].whatNeededToProduce.Add(typeOfGood[current_good], current_quantity);
		}		
		Sql_Init._reader.Close();Sql_Init._reader = null;Sql_Init._command.Dispose();Sql_Init._command = null;
	}

	void Load_Slot_Info(){
		Sql_Init._command = Sql_Init._connection.CreateCommand();
		Sql_Init.sql = "select _id, name, color_r, color_g, color_b from slots";
		Sql_Init._command.CommandText = Sql_Init.sql;Sql_Init._reader = Sql_Init._command.ExecuteReader();
		while (Sql_Init._reader.Read()){
			int		current_id	       = Sql_Init._reader.GetInt32(0);
			string	current_name       = Sql_Init._reader.GetString(1);
			int		current_r	       = Sql_Init._reader.GetInt32(2);
			int		current_g	       = Sql_Init._reader.GetInt32(3);
			int		current_b	       = Sql_Init._reader.GetInt32(4);
			typeOfSlot.Add(current_name, new SlotType(current_id, current_name, current_r, current_g, current_b));
		}		
		Sql_Init._reader.Close();Sql_Init._reader = null;Sql_Init._command.Dispose();Sql_Init._command = null;
	}

	void Load_PeopleTransfer_Info(){
		Sql_Init._command = Sql_Init._connection.CreateCommand();
		Sql_Init.sql = "select _id, name from people_transfer_reasons";
		Sql_Init._command.CommandText = Sql_Init.sql;Sql_Init._reader = Sql_Init._command.ExecuteReader();
		while (Sql_Init._reader.Read()){
			int		current_id	       = Sql_Init._reader.GetInt32(0);
			string	current_name       = Sql_Init._reader.GetString(1);
			typeOfPeopleTransfer.Add(current_name, new PeopleTransferType(current_id, current_name));
		}		
		Sql_Init._reader.Close();Sql_Init._reader = null;Sql_Init._command.Dispose();Sql_Init._command = null;
	}
    
	public static void Refresh_Warehouse_Panel(bool addRandomValue){
		Text_Good_Name.text = "Name";
		Text_Good_Player.text = "Player";
		Text_Good_Region.text = "Region";

		if (addRandomValue == true){
			foreach(KeyValuePair<string, Region> pairRegion in Regions){
				foreach(KeyValuePair<string, GoodType> pairGood in typeOfGood){
					if (pairRegion.Value.region_Real == true){
					    if (pairGood.Value.good_Name != "protection")
					    {
					        pairRegion.Value.warehouse_Reserved[pairGood.Value] = 0;
					    }
					}
					if (pairRegion.Value.region_Name == "Player"){
						pairRegion.Value.warehouse_Reserved[pairGood.Value] = 0;
					}
				}
			}
		}
		foreach(KeyValuePair<string, GoodType> pairGood in typeOfGood){
			if(pairGood.Value.good_Real == true){
				Text_Good_Name.text   += "\n" + pairGood.Value.good_Name;
				Text_Good_Player.text += "\n" + Regions["Player"].warehouse_Reserved[pairGood.Value].ToString();
				Text_Good_Region.text += "\n" + Regions[current_Region].warehouse_Reserved[pairGood.Value].ToString();
			}
		}
	}

	public static string Get_Mats_List(BuildingType currentBuilding){
		string result = "";
		foreach(KeyValuePair<GoodType, int> pairGood in typeOfBuilding[currentBuilding.building_Name].need_4_Build){
			result += pairGood.Key.good_Name + " " + pairGood.Value.ToString() + "\n";
		}
		return result;
	}

	public static void ShowHide_PanelBottomInfo(bool panelState, BuildingType curBuilding){
		if (panelState == true){
			Panel_Bottom_Building_Info.SetActive(true);
			All_Mats_To_Build_Present(curBuilding, true);
		} else {
			Panel_Bottom_Building_Info.SetActive(false);
		}
	}

	public static void ShowHide_FightButton(bool buttonState){
		if (buttonState){
			Button_Fight.SetActive(true);
		} else {
			Button_Fight.SetActive(false);
		}
	}

    public static bool All_Mats_To_Build_Present(BuildingType currentBuilding, bool playerOrNot)
    {
        gm.Text_Materials.text = "Time to build - " + gm.typeOfBuilding[currentBuilding.building_Name].time_to_build.ToString() + "\n\nNeed goods to build\n(stored/need):\n\n";
        gm.Text_Produce.text = "Building can produce:\n\n";

        bool readyToBuild = true;
        if (playerOrNot == true)
        {
            foreach (KeyValuePair<GoodType, int> pair in gm.typeOfBuilding[currentBuilding.building_Name].need_4_Build)
            {
                Text_Materials.text += pair.Key.good_Name + " - " + typeOfBuilding[currentBuilding.building_Name].need_4_Build[pair.Key] + "/" + Regions["Player"].warehouse_Reserved[pair.Key] + "\n";
                if (typeOfBuilding[currentBuilding.building_Name].need_4_Build[pair.Key] > Regions["Player"].warehouse_Reserved[pair.Key])
                {
                    readyToBuild = false;
                }
            }

            foreach (KeyValuePair<GoodType, int> pair in gm.typeOfBuilding[currentBuilding.building_Name].building_produce)
            {
                Text_Produce.text += pair.Key.good_Name + " - " + typeOfBuilding[currentBuilding.building_Name].building_produce[pair.Key] + " per seat\n";
            }
        }

        return readyToBuild;
    }

    public static void Remove_Mats_To_Build(BuildingType currentBuilding, bool playerOrNot)
    {
        if (playerOrNot == true)
        {
            foreach (KeyValuePair<GoodType, int> pair in gm.typeOfBuilding[currentBuilding.building_Name].need_4_Build)
            {
                Regions["Player"].warehouse_Reserved[pair.Key] -= typeOfBuilding[currentBuilding.building_Name].need_4_Build[pair.Key];
            }
        }
    }

	public static void RefreshStats1Window(){
		Stats_Window.text = "";
		
		for (int i = 0; i < Stats.Count; i++){
			if (Stats[i].stat_GameObject.GetComponent<Toggle>().isOn){
				Stats_Window.text += Stats[i].stat_Title;
				Stats_Window.text += Stats[i].statsBody + "\n";
				
			}
		}
	}
	
	public void Show_Economics(){
		if (Panel_Stats.transform.localPosition.y == -480){
			Panel_Stats.transform.localPosition = new Vector3(Panel_Stats.transform.localPosition.x, -120, Panel_Stats.transform.localPosition.z);
		} else {
			Panel_Stats.transform.localPosition = new Vector3(Panel_Stats.transform.localPosition.x, -480, Panel_Stats.transform.localPosition.z);
		}
	}

    public void ShowFightPanel()
    {
        if (Background_Main.transform.position.x == 0)
        {
            Background_Main.transform.position = new Vector3(15, 0, 0);
            Fight_Main.transform.position = new Vector3(0, 0, 0);
        }
        else
        {
            Background_Main.transform.position = new Vector3(0, 0, 0);
            Fight_Main.transform.position = new Vector3(0, 20, 0);
            Refresh_Population_Panel();
            Refresh_Warehouse_Panel(false);
            nt.CheckCompleteArmy();
        }

    }

    public void StartGame()
    {
        Fight_Main.transform.position = new Vector3(0, 20, 0);
        Background_Main.transform.position = new Vector3(0, 0, 0);
        Menu_Main.transform.position = new Vector3(0, -15, 0);
    }

    public static void RearrangeBottomButtonsAndPanels(int section_ID)
    {
        ColorBlock tempColorBlock1 = ColorBlock.defaultColorBlock;
        ColorBlock tempColorBlock2 = ColorBlock.defaultColorBlock;
        tempColorBlock1.normalColor = Color.gray;
        tempColorBlock2.normalColor = baseColor;

        for (int i = 0; i < Bottom_Panels.Count; i++)
        {
            Bottom_Panels[i].SetActive(false);
            Bottom_Buttons[i].GetComponent<Button>().colors = tempColorBlock1;

            if (i == section_ID)
            {
                Bottom_Panels[i].SetActive(true);
                Bottom_Buttons[i].GetComponent<Button>().colors = tempColorBlock2;
                if (i == 1)
                {
                    Panel_Bottom_Taxes_Info.SetActive(true);
                    Refresh_Taxes_Panel(true);
                }
                else
                {
                    Panel_Bottom_Taxes_Info.SetActive(false);
                }

            }

        }
    }

    public void Load_Bottom_Toggles()
    {
        foreach (KeyValuePair<string, Region> pairRegion in Regions)
        {
            if (pairRegion.Value.region_Real == true)
            {
                GameObject tempGameObject = Resources.Load("Prefab/BottomPanel/Toggle_Region_", typeof (GameObject)) as GameObject;
                GameObject new_Info_Panel = GameObject.Instantiate(tempGameObject) as GameObject;
                new_Info_Panel.transform.SetParent(Panel_Content_Items_Regions.transform);
                new_Info_Panel.transform.Find("Label").GetComponent<Text>().text = pairRegion.Value.region_Name;
                new_Info_Panel.GetComponent<Toggle_Region>().choosedItem = pairRegion.Value.region_ID;
                new_Info_Panel.GetComponent<Toggle>().isOn = false;
                new_Info_Panel.transform.localScale = new Vector3(1, 1, 1);
            }
        }


        foreach (KeyValuePair<string, ProductType> pairProduct in typeOfProduct)
        {
            if (pairProduct.Value.product_Real == true)
            {
                GameObject tempGameObject = Resources.Load("Prefab/BottomPanel/Toggle_Product_", typeof(GameObject)) as GameObject;
                GameObject new_Info_Panel = GameObject.Instantiate(tempGameObject) as GameObject;
                new_Info_Panel.transform.SetParent(Panel_Content_Items_Products.transform);
                new_Info_Panel.transform.Find("Label").GetComponent<Text>().text = pairProduct.Value.product_Name;
                new_Info_Panel.GetComponent<Toggle_Product>().choosedItem = pairProduct.Value.product_ID;
                new_Info_Panel.GetComponent<Toggle>().isOn = false;
                new_Info_Panel.transform.localScale = new Vector3(1, 1, 1);
            }
        }


    }

    public static void CheckAllToggles(GameObject PanelWithToggles, bool checkUnCheck)
    {
        foreach (Transform child in PanelWithToggles.transform){
            if (checkUnCheck == true)
            {
                child.GetComponent<Toggle>().isOn = true;
            } 
            else
            {
                child.GetComponent<Toggle>().isOn = false;
            }
        }
    }

    public void ChangeSliderValue()
    {
        if (reset_Taxe == false)
        {
            Text_Percentage_Taxes.text = (Slider_Percentage_Taxes.value * 20).ToString() + "%";
            Sql_Init._command = Sql_Init._connection.CreateCommand();
            Sql_Init.sql = "";

            foreach (Transform childRegion in Panel_Content_Items_Regions.transform)
            {
                {
                    if ((childRegion.gameObject.activeSelf == true) && (childRegion.GetComponent<Toggle>().isOn == true))
                    {
                        foreach (Transform childProduct in Panel_Content_Items_Products.transform)
                        {
                            if ((childProduct.gameObject.activeSelf == true) && (childProduct.GetComponent<Toggle>().isOn == true))
                            {
                                Sql_Init._command = Sql_Init._connection.CreateCommand();
                                Sql_Init.sql +=
                                System.String.Format("UPDATE taxes SET value = {0} WHERE id_regions = {1} and id_products = {2}; " +
                                                     "INSERT INTO taxes(id_regions, id_products, value) SELECT {1}, {2}, {0} WHERE changes() = 0;",
                                                    Slider_Percentage_Taxes.value,
                                                    childRegion.GetComponent<Toggle_Region>().choosedItem,
                                                    childProduct.GetComponent<Toggle_Product>().choosedItem);

                            }
                        }
                    }
                }
            }
            Sql_Init._command.CommandText = Sql_Init.sql;
            Sql_Init._command.ExecuteNonQuery();
            Sql_Init._command.Dispose();
            Sql_Init._command = null;

            Refresh_Taxes_Panel(true);
        }
    }

    public static void Refresh_Taxes_Panel(bool updatePanel)
    {
        Taxes.Clear();
        Sql_Init._command = Sql_Init._connection.CreateCommand();
        Sql_Init.sql = "select id_regions, id_products, value from taxes";
        Sql_Init._command.CommandText = Sql_Init.sql; Sql_Init._reader = Sql_Init._command.ExecuteReader();
        while (Sql_Init._reader.Read())
        {
            int current_id_regions  = Sql_Init._reader.GetInt32(0);
            int current_id_products = Sql_Init._reader.GetInt32(1);
            int current_value       = Sql_Init._reader.GetInt32(2);
            Taxes.Add(new Taxe(current_id_regions, current_id_products, current_value * 20));
        }
        Sql_Init._reader.Close(); Sql_Init._reader = null; Sql_Init._command.Dispose(); Sql_Init._command = null;

        if (updatePanel == true)
        {
            Text_Regions_Left.text = " Regions\n";
            foreach (KeyValuePair<string, Region> pairRegion in Regions)
            {
                if (pairRegion.Value.region_Real == true)
                {
                    Text_Regions_Left.text += " " + pairRegion.Value.region_Name + " \n";
                }
            }

            foreach (KeyValuePair<string, ProductType> pairProduct in typeOfProduct)
            {
                if (pairProduct.Value.product_Real == true)
                {
                    GameObject new_Info_Panel =
                        GameObject.Find("Text_Product_" + pairProduct.Value.product_Name).gameObject;
                    new_Info_Panel.GetComponent<Text>().text = pairProduct.Value.product_Name + "\n";

                    foreach (KeyValuePair<string, Region> pairRegion in Regions)
                    {
                        if (pairRegion.Value.region_Real == true)
                        {
                            bool find_taxe = false;
                            foreach (Taxe currentTaxe in Taxes)
                            {
                                if ((currentTaxe.taxe_region == pairRegion.Value.region_ID) &&
                                    (currentTaxe.taxe_product == pairProduct.Value.product_ID))
                                {
                                    new_Info_Panel.GetComponent<Text>().text += (currentTaxe.taxe_value*20).ToString() +
                                                                                "%\n";
                                    find_taxe = true;
                                }
                            }
                            if (find_taxe == false)
                            {
                                new_Info_Panel.GetComponent<Text>().text += default_tax_rate + "%\n";
                            }
                        }
                    }
                }
            }
        }
    }

    public static void Reset_Slider_Taxe()
    {
        Slider_Percentage_Taxes.value = 0;
        Text_Percentage_Taxes.text = "0%";
        reset_Taxe = false;
    }

    public static void Refresh_Population_Panel()
    {
        int tempPopulation = PeoplesTransferList.Count;

        foreach (KeyValuePair<string, Region> pairRegion in Regions)
        {
            tempPopulation += pairRegion.Value.Peoples.Count;
        }

        Text_Population.text = tempPopulation.ToString();
    }
}