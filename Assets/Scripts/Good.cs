﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Good {


    //public static int Get_Need_Product_In_Region(Region currentRegion, ProductType currentProduct)
    //{
    //    int tempProductQuantity = 0;
    //    foreach (Building building in currentRegion.Buildings)
    //    {
    //        foreach (KeyValuePair<GoodType, int> pairProduce in gm.typeOfBuilding[building.building_Type.building_Name].building_produce)
    //        {
    //            foreach (KeyValuePair<GoodType, int> pairNeed in gm.typeOfGood[pairProduce.Key.good_Name].whatNeededToProduce)
    //            {
    //                if (pairNeed.Key.id_products == currentProduct.product_ID)
    //                {
    //                    tempProductQuantity = pairNeed.Value * building.seatsFilled;
    //                }
    //            }
    //        }
    //    }
    //    return tempProductQuantity;
    //}

	public static int Get_Product_In_Region(Region currentRegion, ProductType currentProduct){
		int tempProductQuantity = 0;
		foreach(KeyValuePair<string, GoodType> pairGood in gm.typeOfGood){
            if (pairGood.Value.id_products == currentProduct.product_ID)
            {
				tempProductQuantity += currentRegion.warehouse_Reserved[pairGood.Value];
			}
		}
		return tempProductQuantity;
	}

    public static int Get_Needs_Product_In_Region(Region currentRegion, ProductType currentProduct){
		int tempProductQuantity = 0;
		foreach(KeyValuePair<string, GoodType> pairGood in gm.typeOfGood){
			if (pairGood.Value.id_products == currentProduct.product_ID){
				tempProductQuantity += currentRegion.region_Needed[pairGood.Value];
			}
		}
		return tempProductQuantity;

	}

	public static void Remove_Product_From_Region_Warehouse(Region currentRegion, ProductType currentProduct, GoodType goodDefault, int productQuantity){
		GoodType maxProduct = goodDefault;
	    int removedQuantity = 0;

	    while (removedQuantity != productQuantity)
	    {
	        int maxProductQuantity = 0;
	        foreach (KeyValuePair<string, GoodType> pairGood in gm.typeOfGood)
	        {
	            if (pairGood.Value.id_products == currentProduct.product_ID)
	            {
	                if (currentRegion.warehouse_Reserved[pairGood.Value] > maxProductQuantity)
	                {
	                    maxProductQuantity = currentRegion.warehouse_Reserved[pairGood.Value];
	                    maxProduct = pairGood.Value;
	                }
	            }
	        }
	        if (currentRegion.warehouse_Reserved[maxProduct] >= productQuantity)
	        {
                removedQuantity = productQuantity;
	            currentRegion.warehouse_Reserved[maxProduct] -= productQuantity;
	        }
	        else
	        {
	            removedQuantity += currentRegion.warehouse_Reserved[maxProduct];
	            currentRegion.warehouse_Reserved[maxProduct] = 0;
	        }
	    }
	}

    public static BuildingType Get_BuildingType_For_Good(GoodType currentGood)
    {
        foreach (KeyValuePair<string, BuildingType> pairBuilding in gm.typeOfBuilding)
        {
            foreach (KeyValuePair<GoodType, int> pairGoodProduce in pairBuilding.Value.building_produce)
            {
                if (pairGoodProduce.Key.good_ID == currentGood.good_ID)
                {
                    return pairBuilding.Value;
                }
            }
        }
        return null;
    }

    public static int Get_TaxRateForGoodInRegion(GoodType currentGoodType, Region currentRegion)
    {
        int tempTaxRate = gm.default_tax_rate;
        gm.Refresh_Taxes_Panel(false);

        foreach (Taxe currentTaxe in gm.Taxes)
        {
            if ((currentTaxe.taxe_region == currentRegion.region_ID) && (currentTaxe.taxe_product == currentGoodType.id_products))
            {
                tempTaxRate = currentTaxe.taxe_value;
            }            
        }

        return tempTaxRate;
    }
}

public class GoodType{
	public int good_ID;
	public string good_Name;
	public bool good_Real;
	public int id_products;

	public Dictionary<GoodType, int> whatNeededToProduce = new Dictionary<GoodType, int>();

	public GoodType(	int 	_good_ID,
	                    string	_good_Name,
	                    bool	_good_Real,
	                	int 	_id_products

	                    )
	{
		good_ID			= _good_ID;
		good_Name		= _good_Name;
		good_Real		= _good_Real;
		id_products		= _id_products;
	}
	
}