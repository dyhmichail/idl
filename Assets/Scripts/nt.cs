﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class nt : MonoBehaviour
{

    public bool resourcesOnWarehouse;
    public List<People> PeopleToTransfer = new List<People>();
    public List<People> PeopleToExit = new List<People>();

//    public static event gm.ShowHideFightButton OnCompleteArmy;

    private void Start()
    {
//        OnCompleteArmy += new gm.ShowHideFightButton(gm.ShowHide_FightButton);
    }

    public void NextTeac()
    {

        for (int i = 0; i < gm.Stats.Count; i++)
        {
            gm.Stats[i].statsBody = "";
        }

        if (gm.Stats[4].stat_GameObject_Enable.GetComponent<Toggle>().isOn) FreeSlotsDistribution(gm.Stats[4]);
        if (gm.Stats[5].stat_GameObject_Enable.GetComponent<Toggle>().isOn) FindPeopleForSlotsInBuildings(gm.Stats[5]);
        Region.ResetAllRegionsNeeded();

        if (gm.Stats[8].stat_GameObject_Enable.GetComponent<Toggle>().isOn) CollectFoodRequests(gm.Stats[8]);
        if (gm.Stats[9].stat_GameObject_Enable.GetComponent<Toggle>().isOn) CollectGoodRequests(gm.Stats[9]);
        if (gm.Stats[10].stat_GameObject_Enable.GetComponent<Toggle>().isOn) CollectProtectRequests(gm.Stats[10]);

        if (gm.Stats[0].stat_GameObject_Enable.GetComponent<Toggle>().isOn) CollectResourcesFromBuildings(gm.Stats[0]);
        if (gm.Stats[1].stat_GameObject_Enable.GetComponent<Toggle>().isOn) PrepareToTransferResources(gm.Stats[1]);
        if (gm.Stats[2].stat_GameObject_Enable.GetComponent<Toggle>().isOn) PeoplesFormOutside(gm.Stats[2]);
        if (gm.Stats[3].stat_GameObject_Enable.GetComponent<Toggle>().isOn)
            RandomPeopleMovingNearRegions(gm.percent_random_people_moving, gm.Stats[3]);

        if (gm.Stats[6].stat_GameObject_Enable.GetComponent<Toggle>().isOn) ShowAllNeeds(gm.Stats[6]);
        if (gm.Stats[7].stat_GameObject_Enable.GetComponent<Toggle>().isOn) ConstructBuildings(gm.Stats[7]);

        ReorganizePeopleTransfers();
        gm.Refresh_Population_Panel();
        gm.RefreshStats1Window();
        gm.Refresh_Warehouse_Panel(false);
        CheckCompleteArmy();
    }

    public void FreeSlotsDistribution(Stat stat)
    {
        bool buildingForSlotFound = false;

        foreach (Slot slot in gm.Slots)
        {
            buildingForSlotFound = false;

            if (slot.slot_BuildingWhereAssigned == null)
            {
                stat.statsBody += "Слот типа " + slot.slot_Type.slot_Name + " ищет место" + "\n";
            }

            if (slot.slot_BuildingWhereAssigned == null)
            {
                foreach (KeyValuePair<string, Region> pairRegion in gm.Regions)
                {
                    foreach (Building building in pairRegion.Value.Buildings)
                    {
                        if ((building.building_Type.building_Slot == slot.slot_Type.slot_ID) && building.construct_Finished)
                        {
                            int haveProtect = Region.RegionProduceProtection(pairRegion.Value);
                            int needProtect = pairRegion.Value.Peoples.Count*gm.need_protection_per_tour;

                            //Debug.Log(haveProtect.ToString() + "|" + needProtect.ToString());
                            if (((building.slotsFilled <
                                  gm.typeOfBuilding[building.building_Type.building_Name].seatsQuantity) &&
                                 (Building.Building_Produce_Protection(building) == false))
                                ||
                                ((Building.Building_Produce_Protection(building) == true) && (haveProtect < needProtect)))
                            {
                                building.slotsFilled++;
                                slot.slot_BuildingWhereAssigned = building;
                                slot.slot_GameObject.transform.SetParent(building.building_PeoplesDock.transform);
                                slot.slot_GameObject.transform.localScale = new Vector3(1, 1, 1);
                                buildingForSlotFound = true;
                                stat.statsBody += "Слот типа " + slot.slot_Type.slot_Name +
                                                  " нашёл место в здании " + building.building_Name +
                                                  " в регионе " + building.building_RegionWherePlaced.region_Name + "\n";
                            }
                        }
                        // Construction Block
                        if ((slot.slot_Type.slot_ID == gm.typeOfSlot["ConstructionWorker"].slot_ID) &&
                            (building.construct_Finished == false))
                        {
                            if (building.slotsFilled <
                                gm.typeOfBuilding[building.building_Type.building_Name].seatsQuantity)
                            {
                                building.slotsFilled++;
                                slot.slot_BuildingWhereAssigned = building;
                                slot.slot_GameObject.transform.SetParent(building.building_PeoplesDock.transform);
                                slot.slot_GameObject.transform.localScale = new Vector3(1, 1, 1);
                                buildingForSlotFound = true;
                                stat.statsBody += "Слот типа " + slot.slot_Type.slot_Name +
                                                  " нашёл место в здании " + building.building_Name +
                                                  " в регионе " + building.building_RegionWherePlaced.region_Name + "\n";

                            }
                        }

                        if (buildingForSlotFound) break;
                    }
                    if (buildingForSlotFound) break;
                }
            }

            if ((buildingForSlotFound == false) && (slot.slot_BuildingWhereAssigned == null) &&
                (slot.slot_Type != gm.typeOfSlot["ConstructionWorker"]))
            {
                var tempNeed = Need.Add_Need(Slot.Get_Building_For_SlotType(slot.slot_Type));
            }
        }
    }

    public void FindPeopleForSlotsInBuildings(Stat stat)
    {
        bool peopleForSlotFound;
        foreach (Slot slot in gm.Slots)
        {
            if ((slot.slot_BuildingWhereAssigned != null) && (slot.slot_PeopleAssigned == null))
            {
                People tempWorker = null;
                tempWorker = TryToFindWorkerInThisRegion(slot.slot_BuildingWhereAssigned.building_RegionWherePlaced);
                if (tempWorker != null)
                {
                    slot.slot_PeopleAssigned = tempWorker;
                    tempWorker.people_Slot = slot;
                    slot.slot_BuildingWhereAssigned.seatsFilled++;
                    tempWorker.people_GameObject.transform.SetParent(slot.slot_GameObject.transform);
                    tempWorker.people_GameObject.transform.localScale = new Vector3(1, 1, 1);
                    tempWorker.people_GameObject.transform.localPosition = new Vector3(0, 0, 0);
                    tempWorker.people_GameObject.transform.GetComponent<RectTransform>().offsetMax = new Vector2(200, 0);
                    tempWorker.people_GameObject.transform.GetComponent<RectTransform>().offsetMin = new Vector2(0, -200);


                    if (slot.slot_Reserved == true)
                    {
                        slot.slot_Reserved = false;
                    }
                    if (slot.slot_Type == gm.typeOfSlot["ConstructionWorker"])
                    {
                        slot.slot_BuildingWhereAssigned.contructionWorkerHere++;
                    }
                }
                else
                {
                    if (slot.slot_Reserved == false)
                    {
                        peopleForSlotFound = false;
                        foreach (KeyValuePair<string, Region> pairRegion in gm.Regions)
                        {
                            tempWorker = TryToFindWorkerInAnotherRegion(pairRegion.Value);
                            if (tempWorker != null)
                            {
                                PeopleTransfer.TransferCitizenFromTo(
                                    slot.slot_BuildingWhereAssigned.building_RegionWherePlaced, tempWorker,
                                    gm.typeOfPeopleTransfer["FindWorkInAnotherRegion"]);
                                slot.slot_Reserved = true;
                                slot.slot_BuildingWhereAssigned.seatsReserved++;
                                peopleForSlotFound = true;
                            }
                            if (peopleForSlotFound) break;
                        }
                    }
                }
            }
        }
        PeopleTransfer.ShowTransfersOnReason(gm.typeOfPeopleTransfer["FindWorkInAnotherRegion"], stat);
    }

    public People TryToFindWorkerInThisRegion(Region regionFrom)
    {
        for (int i = 0; i < regionFrom.Peoples.Count; i++)
        {
            if (regionFrom.Peoples[i].people_Slot == null)
            {
                return regionFrom.Peoples[i];
            }
        }
        return null;
    }

    public People TryToFindWorkerInAnotherRegion(Region regionFrom)
    {
        foreach (KeyValuePair<string, Region> pairRegion in gm.Regions)
        {
            if (pairRegion.Value != regionFrom)
            {
                for (int i = 0; i < pairRegion.Value.Peoples.Count; i++)
                {
                    if (pairRegion.Value.Peoples[i].people_Slot == null)
                    {
                        return pairRegion.Value.Peoples[i];
                    }
                }
            }
        }
        return null;
    }

    public void CollectFoodRequests(Stat stat)
    {
        Region regionMoveTo = GetRegionWhereMaximumFood();
        foreach (KeyValuePair<string, Region> pairRegion in gm.Regions)
        {
            if (pairRegion.Value.region_Real)
            {
                PeopleToTransfer.Clear();
                PeopleToExit.Clear();

                foreach (People people in pairRegion.Value.Peoples)
                {
                    if (Good.Get_Product_In_Region(pairRegion.Value, gm.typeOfProduct["Food"]) >= gm.eat_per_tour)
                    {
                        Good.Remove_Product_From_Region_Warehouse(pairRegion.Value, gm.typeOfProduct["Food"],
                            gm.typeOfGood["grain"], gm.eat_per_tour);
                        people.food_limit_now = 0;
                    }
                    else
                    {
                        people.food_limit_now += gm.eat_per_tour;
                    }
                    if (people.people_Slot == null)
                    {
                        if (people.food_limit_now > people.food_limit)
                        {
                            if (regionMoveTo != gm.Regions["Outside"])
                            {
                                stat.statsBody += "У человека " + people.people_Name +
                                                  "превышен лимит по еде! Переходит в " + regionMoveTo.region_Name +
                                                  "\n";
                                people.food_limit_now += regionMoveTo.timeToDelivery;
                                PeopleToTransfer.Add(people);
                            }
                            else
                            {
                                stat.statsBody += "У человека " + people.people_Name +
                                                  "превышен лимит по еде! Переходить некуда. Инициируем слот.\n";
                                Slot.Add_Slot(gm.typeOfSlot["Farmer"], false);
                            }
                        }
                        if (people.food_limit_now > people.food_limit*2)
                        {
                            PeopleToExit.Add(people);
                            stat.statsBody += "Человек " + people.people_Name + " ушёл навсегда!\n";
                        }
                    }
                }
                foreach (People people in PeopleToTransfer)
                {
                    PeopleTransfer.TransferCitizenFromTo(regionMoveTo, people,
                        gm.typeOfPeopleTransfer["FindRegionWithFood"]);
                }
                foreach (People people in PeopleToExit)
                {
                    people.people_RegionWherePlaced.Peoples.Remove(people);
                    people.people_RegionWherePlaced = null;
                    people.people_GameObject.transform.SetParent(gm.PanelLeavedPeoples.transform);
                    people.people_GameObject.transform.localScale = new Vector3(1, 1, 1);
                }
                stat.statsBody += "Регион " + pairRegion.Value.region_Name + " потребляет " + pairRegion.Value.Peoples.Count*gm.eat_per_tour + " еды\n";
            }
        }
    }


    public Region GetRegionWhereMaximumFood()
    {
        Region maxRegion = gm.Regions["Outside"];
        int maxProduct = 0;

        foreach (KeyValuePair<string, Region> pairRegion in gm.Regions)
        {
            if (pairRegion.Value.region_Real)
            {
                int tempHave = Good.Get_Product_In_Region(pairRegion.Value, gm.typeOfProduct["Food"]);
                int tempNeed = pairRegion.Value.Peoples.Count*gm.eat_per_tour;
                tempNeed += Good.Get_Needs_Product_In_Region(pairRegion.Value, gm.typeOfProduct["Food"]);

                if (((tempHave - tempNeed) > maxProduct) && ((tempHave - tempNeed) > 0))
                {
                    maxProduct = (tempHave - tempNeed);
                    maxRegion = pairRegion.Value;
                }
            }
        }
        return maxRegion;
    }

    public Region GetRegionWhereMaximumGood()
    {
        Region maxRegion = gm.Regions["Outside"];
        int maxProduct = 0;

        foreach (KeyValuePair<string, Region> pairRegion in gm.Regions)
        {
            if (pairRegion.Value.region_Real)
            {
                int tempHave = Good.Get_Product_In_Region(pairRegion.Value, gm.typeOfProduct["Goods"]);
                int tempNeed = pairRegion.Value.Peoples.Count*gm.use_goods_per_tour;
                tempNeed += Good.Get_Needs_Product_In_Region(pairRegion.Value, gm.typeOfProduct["Goods"]);

                if (((tempHave - tempNeed) > maxProduct) && ((tempHave - tempNeed) > 0))
                {
                    maxProduct = (tempHave - tempNeed);
                    maxRegion = pairRegion.Value;
                }
            }
        }
        return maxRegion;
    }

    public Region GetRegionWhereMaximumProtection()
    {
        Region maxRegion = gm.Regions["Outside"];
        int maxProduct = 0;

        foreach (KeyValuePair<string, Region> pairRegion in gm.Regions)
        {
            if (pairRegion.Value.region_Real)
            {
                int tempHave = Good.Get_Product_In_Region(pairRegion.Value, gm.typeOfProduct["Protection"]);
                int tempNeed = pairRegion.Value.Peoples.Count*gm.need_protection_per_tour;

                if (((tempHave - tempNeed) > maxProduct) && ((tempHave - tempNeed) > 0))
                {
                    maxProduct = (tempHave - tempNeed);
                    maxRegion = pairRegion.Value;
                }
            }
        }
        return maxRegion;
    }


    public void CollectGoodRequests(Stat stat)
    {
        Region regionMoveTo = GetRegionWhereMaximumGood();
        foreach (KeyValuePair<string, Region> pairRegion in gm.Regions)
        {
            if (pairRegion.Value.region_Real)
            {
                PeopleToTransfer.Clear();
                PeopleToExit.Clear();

                foreach (People people in pairRegion.Value.Peoples)
                {
                    if (!people.people_InArmyNow)
                    {
                        if (Good.Get_Product_In_Region(pairRegion.Value, gm.typeOfProduct["Goods"]) >=
                            gm.use_goods_per_tour)
                        {
                            Good.Remove_Product_From_Region_Warehouse(pairRegion.Value, gm.typeOfProduct["Goods"],
                                gm.typeOfGood["money"], gm.use_goods_per_tour);
                            people.goods_limit_now = 0;
                        }
                        else
                        {
                            people.goods_limit_now += gm.use_goods_per_tour;
                        }
                        if (people.people_Slot == null)
                        {
                            if (people.goods_limit_now > people.goods_limit)
                            {
                                if (regionMoveTo != gm.Regions["Outside"])
                                {
                                    stat.statsBody += "У человека " + people.people_Name +
                                                      "превышен лимит по благам! Переходит в " +
                                                      regionMoveTo.region_Name + "\n";
                                    people.goods_limit_now += regionMoveTo.timeToDelivery;
                                    PeopleToTransfer.Add(people);
                                }
                                else
                                {
                                    stat.statsBody += "У человека " + people.people_Name +
                                                      "превышен лимит по благам! Переходить некуда. Инициируем слот.\n";
                                    Slot.Add_Slot(gm.typeOfSlot["Worker"], false);
                                }
                            }
                            if (people.goods_limit_now > people.goods_limit*2)
                            {
                                stat.statsBody += "Человек " + people.people_Name + " готов идти воевать!\n";
                                PeopleToExit.Add(people);
                                people.people_InArmyNow = true;
                            }
                        }
                    }
                }
                foreach (People people in PeopleToTransfer)
                {
                    PeopleTransfer.TransferCitizenFromTo(regionMoveTo, people,
                        gm.typeOfPeopleTransfer["FindRegionWithGoods"]);
                }
                foreach (People people in PeopleToExit)
                {
                    gm.PeoplesReadyToWar.Add(people);
                }
                stat.statsBody += "Регион " + pairRegion.Value.region_Name + " потребляет " + pairRegion.Value.Peoples.Count * gm.use_goods_per_tour + " благ\n";
            }
        }
        stat.statsBody += "В отряде уже " + CheckCompleteArmy().ToString() + "\n";
    }

    public static int CheckCompleteArmy()
    {
        int tempReadyToWar = 0;
        foreach (People people in gm.PeoplesReadyToWar)
        {
            tempReadyToWar++;
        }

        if (tempReadyToWar >= gm.min_army)
        {
            gm.ShowHide_FightButton(true);
        }
        else
        {
            gm.ShowHide_FightButton(false);
        }

        gm.Text_Population.text += "|" + tempReadyToWar;
        return tempReadyToWar;
    }

    public void CollectProtectRequests(Stat stat)
    {


        Region regionMoveTo = GetRegionWhereMaximumProtection();
        foreach (KeyValuePair<string, Region> pairRegion in gm.Regions)
        {
            if (pairRegion.Value.region_Real)
            {
                PeopleToTransfer.Clear();
                PeopleToExit.Clear();
                int tempHave = Region.RegionProduceProtection(pairRegion.Value);

                int tempNeed = pairRegion.Value.Peoples.Count*gm.need_protection_per_tour;
                stat.statsBody += "Регион " + pairRegion.Value.region_Name + " производит " + tempHave + " требует " + tempNeed + " защиты\n";

                foreach (People people in pairRegion.Value.Peoples)
                {
                    if (tempHave > tempNeed)
                    {
                        people.protect_limit_now = 0;
                    }
                    else
                    {
                        people.protect_limit_now += gm.need_protection_per_tour;
                        if (people.protect_limit_now > people.protect_limit)
                        {
                            if ((regionMoveTo != gm.Regions["Outside"]) && (regionMoveTo != people.people_RegionWherePlaced))
                            {
                                stat.statsBody += "У человека " + people.people_Name + "превышен лимит по защите! Переходит в " + regionMoveTo.region_Name +
                                                  "\n";
                                people.protect_limit_now += regionMoveTo.timeToDelivery;
                                PeopleToTransfer.Add(people);
                            }
                            else
                            {
                                stat.statsBody += "У человека " + people.people_Name + "превышен лимит по защите! Переходить некуда. Инициируем слот.\n";
                                Slot.Add_Slot(gm.typeOfSlot["Soldier"], false);
                            }
                            if (people.protect_limit_now > people.protect_limit*2)
                            {
                                PeopleToExit.Add(people);
                                stat.statsBody += "Человек " + people.people_Name + " ушёл навсегда!\n";
                            }
                        }
                    }
                }


                foreach (People people in PeopleToTransfer)
                {
                    PeopleTransfer.TransferCitizenFromTo(regionMoveTo, people,
                        gm.typeOfPeopleTransfer["FindRegionWithProtect"]);
                }
                foreach (People people in PeopleToExit)
                {
                    people.people_RegionWherePlaced.Peoples.Remove(people);
                    people.people_RegionWherePlaced = null;
                    people.people_GameObject.transform.SetParent(gm.PanelLeavedPeoples.transform);
                    people.people_GameObject.transform.localScale = new Vector3(1, 1, 1);
                }
                //stat.statsBody += "Регион " + pairRegion.Value.region_Name + " потребляет " + pairRegion.Value.Peoples.Count*gm.need_protection_per_tour + " защиты\n";


                if (tempHave < tempNeed)
                {
                    Building patrolBuilding = null;
                    Slot tempSlotClass = null;
                    foreach (Building building in pairRegion.Value.Buildings)
                    {
                        if (Building.Building_Produce_Protection(building))
                        {
                            patrolBuilding = building;
                        }
                    }

                    int protectProduceTheory = patrolBuilding.slotsFilled * patrolBuilding.building_Type.building_produce[gm.typeOfGood["protection"]];

                    stat.statsBody += "Всего слотов в патруле " + patrolBuilding.slotsFilled + " могут производить " + protectProduceTheory.ToString() + "\n";
                    int howMachSlotsNeed = (int) Mathf.Ceil((tempNeed - protectProduceTheory)/(float)patrolBuilding.building_Type.building_produce[gm.typeOfGood["protection"]]);
                    stat.statsBody += "Региону " + pairRegion.Value.region_Name + " не хватает " + howMachSlotsNeed + " слотов защиты!\n";

                    //tempSlotClass = Slot.Add_Slot(gm.typeOfSlot["Soldier"], false);
                    //if (tempSlotClass != null)
                    //{
                    //    patrolBuilding.slotsFilled++;
                    //    tempSlotClass.slot_BuildingWhereAssigned = PatrolBuilding;
                    //    tempSlotClass.slot_GameObject.transform.SetParent(PatrolBuilding.building_PeoplesDock.transform);
                    //    tempSlotClass.slot_GameObject.transform.localScale = new Vector3(1, 1, 1);
                    //}
                }
            }
        }
    }

    public void CollectResourcesFromBuildings(Stat stat)
    {
        Region.ResetAllRegionsNeeded();
        foreach (KeyValuePair<string, Region> pairRegion in gm.Regions)
        {
            foreach (Building building in pairRegion.Value.Buildings)
            {
                resourcesOnWarehouse = true;
                string tempString = "";
                foreach (KeyValuePair<GoodType, int> pairProduce in gm.typeOfBuilding[building.building_Type.building_Name].building_produce)
                {
                    foreach (KeyValuePair<GoodType, int> pairNeed in gm.typeOfGood[pairProduce.Key.good_Name].whatNeededToProduce)
                    {
                        int buildingNeedResources = pairNeed.Value*building.seatsFilled;
                        if (buildingNeedResources > pairRegion.Value.warehouse_Reserved[pairNeed.Key])
                        {
                            resourcesOnWarehouse = false;
                            Slot.Add_Slot(Slot.Get_Slot_Type_On_ID(Good.Get_BuildingType_For_Good(pairNeed.Key).building_Slot), false);
                        }
                        if ((buildingNeedResources > 0) && resourcesOnWarehouse)
                        {
                            tempString += "Здание " + building.building_Name + " получило " +
                                          buildingNeedResources.ToString() + " " + pairNeed.Key.good_Name + " для производства " + pairProduce.Key.good_Name + "\n";
                        }
                    }
                }
                if (resourcesOnWarehouse)
                {
                    stat.statsBody += tempString;
                    foreach (
                        KeyValuePair<GoodType, int> pairProduce in
                            gm.typeOfBuilding[building.building_Type.building_Name].building_produce)
                    {
                        foreach (
                            KeyValuePair<GoodType, int> pairNeed in
                                gm.typeOfGood[pairProduce.Key.good_Name].whatNeededToProduce)
                        {
                            pairRegion.Value.warehouse_Reserved[pairNeed.Key] -= pairNeed.Value*building.seatsFilled;
                        }

                        int produced = pairProduce.Value*building.seatsFilled;
                        int currentTaxes = Good.Get_TaxRateForGoodInRegion(pairProduce.Key, pairRegion.Value);
                        int goodTaxes = (int)(produced * currentTaxes / 100f);

                        //pairRegion.Value.warehouse_Reserved[pairProduce.Key] += (produced - goodTaxes);

                        pairRegion.Value.warehouse_Reserved[pairProduce.Key] += pairProduce.Key.good_Name != "protection" ? (produced - goodTaxes) : produced;

                        
                        gm.Regions["Player"].warehouse_Reserved[pairProduce.Key] += pairProduce.Key.good_Name != "protection" ? goodTaxes : 0;

                        stat.statsBody += "Здание " + building.building_Name + " произвело " + produced.ToString() + " " + pairProduce.Key.good_Name + "\n";
                        if (pairProduce.Key.good_Name != "protection")
                        {
                            stat.statsBody += "Налоги (" + currentTaxes + "%) - " + goodTaxes + " " + pairProduce.Key.good_Name + "\n";
                        }

                    }
                }
            }
            stat.statsBody += "----------------------\n";
        }
    }

    public void PrepareToTransferResources(Stat stat)
    {

        foreach (KeyValuePair<string, Region> pairRegion in gm.Regions)
        {
            foreach (KeyValuePair<string, GoodType> pairGoodType in gm.typeOfGood)
            {
                if (pairGoodType.Value.good_Real && pairRegion.Value.region_Real)
                {
                    int currentNeeded = pairRegion.Value.region_Needed[pairGoodType.Value];
                    int currentHave = pairRegion.Value.warehouse_Reserved[pairGoodType.Value];
                    if (currentNeeded < currentHave)
                    {
                        int deliveryGoodsQuantity = (int) Mathf.Floor((currentHave - currentNeeded)*gm.percent_extra_goods_to_transfer/100f);
                        if (deliveryGoodsQuantity >= gm.min_goods_to_transfer)
                        {
                            deliveryGoodsQuantity =
                                (int)
                                    (((deliveryGoodsQuantity - (deliveryGoodsQuantity%gm.min_goods_to_transfer))/
                                      (float) gm.min_goods_to_transfer)*gm.min_goods_to_transfer);
                            if (pairRegion.Value != gm.Regions["Castle"])
                            {
                                gm.deliveryTaskList.Add(new DeliveryTask(pairRegion.Value, gm.Regions["Castle"],
                                    pairGoodType.Value, deliveryGoodsQuantity, pairRegion.Value.timeToDelivery, false));
                                pairRegion.Value.warehouse_Reserved[pairGoodType.Value] -= deliveryGoodsQuantity;
                            }
                            else
                            {
                                Region regionWhom = gm.Regions["Castle"];
                                foreach (KeyValuePair<string, Region> pairRegion2 in gm.Regions)
                                {
                                    if (pairRegion2.Value.region_Real == true)
                                    {
                                        if (regionWhom.warehouse_Reserved[pairGoodType.Value] >
                                            pairRegion2.Value.warehouse_Reserved[pairGoodType.Value])
                                        {
                                            regionWhom = pairRegion2.Value;
                                        }
                                        else if (regionWhom.warehouse_Reserved[pairGoodType.Value] ==
                                                 pairRegion2.Value.warehouse_Reserved[pairGoodType.Value])
                                        {
                                            regionWhom = Random.Range(0f, 1f) > 0.5f
                                                ? regionWhom
                                                : pairRegion2.Value;
                                        }

                                    }
                                }
                                stat.statsBody += "Минимум " + pairGoodType.Value.good_Name + " в регионе " + regionWhom.region_Name + "\n";
                                gm.deliveryTaskList.Add(new DeliveryTask(gm.Regions["Castle"], regionWhom, pairGoodType.Value, deliveryGoodsQuantity, regionWhom.timeToDelivery, false));
                                gm.Regions["Castle"].warehouse_Reserved[pairGoodType.Value] -= deliveryGoodsQuantity;
                            }
                        }
                    }
                }
            }
        }

        foreach (DeliveryTask delivery in gm.deliveryTaskList)
        {
            stat.statsBody += delivery.delivery_ID.ToString() + " | " +
                              delivery.fromDelivery.region_Name.ToString() +
                              " > > " + delivery.whomDelivery.region_Name.ToString() + " | " +
                              delivery.typeOfGoodsForDelivery.good_Name + " - " +
                              "" + delivery.quantytiGoodsToDelivery.ToString() + " - " +
                              " время - " + delivery.timeRestForDelivery.ToString() + "\n";
            if (delivery.timeRestForDelivery > 0)
            {
                delivery.timeRestForDelivery--;
            }
            else
            {
                delivery.whomDelivery.warehouse_Reserved[delivery.typeOfGoodsForDelivery] +=
                    delivery.quantytiGoodsToDelivery;
                delivery.toDelete = true;
            }
        }

        for (int i = (gm.deliveryTaskList.Count - 1); i >= 0; i--)
        {
            if (gm.deliveryTaskList[i].toDelete)
            {
                gm.deliveryTaskList.RemoveAt(i);
            }
        }
    }

    public void PeoplesFormOutside(Stat stat)
    {
        int summaryNeededFood = 0;
        int summaryHadFood = 0;
        int summaryPopulation = 0;

        foreach (KeyValuePair<string, Region> pairRegion in gm.Regions)
        {
            summaryNeededFood += pairRegion.Value.Peoples.Count*gm.eat_per_tour;
            summaryPopulation += pairRegion.Value.Peoples.Count;
            if (pairRegion.Value.region_Real == true)
            {
                summaryHadFood += Good.Get_Product_In_Region(pairRegion.Value, gm.typeOfProduct["Food"]);
            }
        }

        summaryPopulation += gm.PeoplesTransferList.Count;

        int howManyPeopleAdd = 0;
        if (summaryHadFood > summaryNeededFood)
        {
            howManyPeopleAdd =
                (int) Mathf.Floor((summaryHadFood - summaryNeededFood)/(float) gm.add_people_per_food_quantity);

            if (gm.max_population < (summaryPopulation + howManyPeopleAdd))
            {
                howManyPeopleAdd = gm.max_population - summaryPopulation;
                if (howManyPeopleAdd < 0)
                {
                    howManyPeopleAdd = 0;
                }
            }

            stat.statsBody += "Население в городе - " + summaryPopulation.ToString()
                              + " Максимальное население - " + gm.max_population.ToString()
                              + "\nВсего Food на складах - " + summaryHadFood.ToString()
                              + " Всего нужно FOOD - " + summaryNeededFood.ToString()
                              + " Излишек FOOD - " + (summaryHadFood - summaryNeededFood).ToString()
                              + "\nПрийдёт " + howManyPeopleAdd.ToString()
                              + " По однму на каждые " + gm.add_people_per_food_quantity.ToString() + " излишков еды\n";

            if (howManyPeopleAdd > 0)
            {
                for (int i = 0; i < howManyPeopleAdd; i++)
                {
                    People tempPeopleClass = People.Add_People(gm.Regions["Outside"], true);
                    PeopleTransfer.TransferCitizenFromTo(gm.Regions["Castle"], tempPeopleClass,
                        gm.typeOfPeopleTransfer["FromOutside"]);
                }
            }
        }
        PeopleTransfer.ShowTransfersOnReason(gm.typeOfPeopleTransfer["FromOutside"], stat);
    }

    public void RandomPeopleMovingNearRegions(int percentageToMove, Stat stat)
    {

        foreach (KeyValuePair<string, Region> pairRegion in gm.Regions)
        {
            int regionSummmaryFreePeoples = 0;
            for (int i = 0; i < pairRegion.Value.Peoples.Count; i++)
            {
                if (pairRegion.Value.Peoples[i].people_Slot == null)
                {
                    regionSummmaryFreePeoples++;
                }
            }
            int howManyPeoplesToMove = 0;
            if (regionSummmaryFreePeoples > 0)
            {
                howManyPeoplesToMove = (int) Mathf.Floor(percentageToMove * regionSummmaryFreePeoples / 100f);
                stat.statsBody += "В регионе " + pairRegion.Value.region_Name + " свободных " +
                                  regionSummmaryFreePeoples.ToString() + " Процент перемещения - " +
                                  percentageToMove.ToString() + " Будет перемещено - " + howManyPeoplesToMove.ToString() +
                                  "\n";
            }
            if (howManyPeoplesToMove > 0)
            {
                Region tempRegion = pairRegion.Value;
                while ((tempRegion == pairRegion.Value) || (tempRegion.region_Real != true))
                {
                    List<string> keyList = new List<string>(gm.Regions.Keys);
                    string randomKey = keyList[Random.Range(0, gm.Regions.Count)];
                    tempRegion = gm.Regions[randomKey];
                }

                stat.statsBody += "Случайно выбранный район для перемещения - " + tempRegion.region_Name + "\n";
                for (int j = 0; j < howManyPeoplesToMove; j++)
                {
                    for (int i = 0; i < pairRegion.Value.Peoples.Count; i++)
                    {
                        if (pairRegion.Value.Peoples[i].people_Slot == null)
                        {
                            PeopleTransfer.TransferCitizenFromTo(tempRegion, pairRegion.Value.Peoples[i],
                                gm.typeOfPeopleTransfer["RandomTransfer"]);
                            break;
                        }
                    }
                }
            }
        }
        PeopleTransfer.ShowTransfersOnReason(gm.typeOfPeopleTransfer["RandomTransfer"], stat);
    }
   
    public void ShowAllNeeds(Stat stat)
    {
        Building tempBuilding = null;
		foreach(Need need in gm.Needs){
		    stat.statsBody += need.need_ID + " | " + stat.statsBody + need.need_Name + " | " + stat.statsBody + need.need_Type + "\n";

            if (need.need_BuildingConstruct == null)
            {
                if (Building.Get_Region_With_Free_Collider_For_Build() != null)
                {
                    tempBuilding = Building.Add_Building(Building.Get_Region_With_Free_Collider_For_Build(), need.need_BuildingConstructType, true, null);
                    need.need_BuildingConstruct = tempBuilding;
                    need.need_BuildingConstruct.constructionSlotsRequested = false;
                }
                else
                {
                    stat.statsBody += "Не осталось свободных коллайдеров для постройки зданий!\n";
                }
            } else {
				stat.statsBody += need.need_BuildingConstruct.building_Name.ToString() + "\n";
				stat.statsBody += need.need_BuildingConstruct.seatsFilled.ToString() + "|" + need.need_BuildingConstruct.seatsReserved.ToString() + "\n";
                if (need.need_BuildingConstruct.construct_Finished == true)
                {
                    need.need_ToDelete = true;
                }
			}
		}
		
		for(int i = (gm.Needs.Count - 1); i >= 0 ; i--){
			if (gm.Needs[i].need_ToDelete){
				gm.Needs.RemoveAt(i);
			}
		}
    }
    
    public void ConstructBuildings(Stat stat)
    {
        bool allMaterialsAviable;

        foreach (KeyValuePair<string, Region> pairRegion in gm.Regions)
        {
            foreach (Building building in pairRegion.Value.Buildings)
            {
                if (building.construct_Finished == false)
                {
                    stat.statsBody += "Строительство " + building.building_Name + " | " + building.building_Type.building_Name + " прогресс:" + "\n";
                    foreach (KeyValuePair<GoodType, int> pairGoodNeed in building.building_Type.need_4_Build)
                    {
                        stat.statsBody += "Необходимо " + pairGoodNeed.Key.good_Name + " | " + pairGoodNeed.Value + "\n";

                        if (building.building_RegionWherePlaced.warehouse_Reserved[pairGoodNeed.Key] > 0)
                        {
                            int howMatchNeeded = pairGoodNeed.Value - building.whatNeedToConstructStored[pairGoodNeed.Key];
                            int howMatchHas = building.building_RegionWherePlaced.warehouse_Reserved[pairGoodNeed.Key];
                            stat.statsBody += "Ещё нужно " + pairGoodNeed.Key.good_Name + " | " + howMatchNeeded + "\n";

                            if (howMatchHas >= howMatchNeeded)
                            {
                                building.building_RegionWherePlaced.warehouse_Reserved[pairGoodNeed.Key] -= howMatchNeeded;
                                building.whatNeedToConstructStored[pairGoodNeed.Key] += howMatchNeeded;
                            }
                            else
                            {
                                building.building_RegionWherePlaced.warehouse_Reserved[pairGoodNeed.Key] = 0;
                                building.whatNeedToConstructStored[pairGoodNeed.Key] += howMatchHas;
                            }
                            stat.statsBody += "------- После перемещения на стройку стало\n";
                            stat.statsBody += "Есть у здания " + pairGoodNeed.Key.good_Name + " | " + building.whatNeedToConstructStored[pairGoodNeed.Key] + "\n";
                            stat.statsBody += "Осталось на складе " + pairGoodNeed.Key.good_Name  + " | " + building.building_RegionWherePlaced.warehouse_Reserved[pairGoodNeed.Key].ToString() + "\n\n";

                        }
                    }

                    allMaterialsAviable = true;
                    foreach (KeyValuePair<GoodType, int> pairGoodNeed in building.building_Type.need_4_Build)
                    {
                        if (pairGoodNeed.Value > building.whatNeedToConstructStored[pairGoodNeed.Key])
                        {
                                allMaterialsAviable = false;
                        }
                    }

                    if (allMaterialsAviable)
                    {
                        stat.statsBody += "Все материалы для постройки в наличии!\n";
                        stat.statsBody += "Необходимо " + building.building_Type.builders_Need.ToString() + "строителей" + "\n";
                        stat.statsBody += "В наличии " + building.contructionWorkerHere.ToString() + " строителей" + "\n";

                        if (building.constructionSlotsRequested == false)
                        {
                            for (int i = 0; i < building.building_Type.builders_Need; i++)
                            {
                                Slot.Add_Slot(gm.typeOfSlot["ConstructionWorker"], true);
                            }
                            building.constructionSlotsRequested = true;
                        }
                        building.building_GameObject.GetComponent<Construct_Mouse_Over_Collider>().startConstruct = true;
                    }

                    building.constructionProgress += building.contructionWorkerHere;
                    if (building.constructionProgress >= building.building_Type.time_to_build)
                    {
                        foreach (Slot slot in gm.Slots)
                        {
                            if (slot.slot_BuildingWhereAssigned == building)
                            {
                                Debug.Log(building.building_Name + "|"+slot.slot_Name);
                                if (slot.slot_PeopleAssigned != null)
                                {
                                    slot.slot_PeopleAssigned.people_Slot = null;
                                    if (slot.slot_PeopleAssigned.people_GameObject != null)
                                    {
                                        slot.slot_PeopleAssigned.people_GameObject.transform.SetParent(slot.slot_PeopleAssigned.people_RegionWherePlaced.region_PeoplesDock.transform);
                                        slot.slot_PeopleAssigned.people_GameObject.transform.localScale = new Vector3(1, 1, 1);
                                    }
                                }
                                slot.slot_GameObject.transform.SetParent(null);
                                slot.slot_ToDelete = true;
                            }
                        }
                        building.construct_Finished = true;
                        stat.statsBody += "\n>>> Здание " + building.building_Name + " ПОСТРОЕНО!" + "\n";
                        building.ProgressBar.GetComponent<Image>().fillAmount = 0f;
                        building.slotsFilled = 0;
                    }
                    else
                    {
                        stat.statsBody += "\n>>> Здание " + building.building_Name + " строится " + building.contructionWorkerHere + " рабочими  прогресс " + building.constructionProgress.ToString() + "/" + building.building_Type.time_to_build + "\n\n";
                        if (building.buildCurrentProgress == 0)
                        {
                            building.ProgressBar_Under.SetActive(true);
                            building.spriteColor = new Color(1f, 1f, 1f, 0f);
                            building.building_GameObject.GetComponent<SpriteRenderer>().color = building.spriteColor;
                        }
                        if (building.constructInProgress == false)
                        {
                            if (building.building_Type.time_to_build > building.buildCurrentProgress)
                            {
                                StartCoroutine(ConstructBuilding(building, building.contructionWorkerHere, true));
                                building.ProgressBar.GetComponent<Image>().fillAmount = building.buildCurrentProgress/(float)building.building_Type.time_to_build;
                                building.spriteColor = new Color(1f, 1f, 1f, building.buildCurrentProgress/(float) building.building_Type.time_to_build);
                                building.building_GameObject.GetComponent<SpriteRenderer>().color = building.spriteColor;
                                building.ProgressBar_Under.SetActive(false);
                            }
                            else
                            {
                                building.spriteColor = new Color(1f, 1f, 1f, 1f);
                            }
                        }
                    }

                    for (int i = (gm.Slots.Count - 1); i >= 0; i--)
                    {
                        if (gm.Slots[i].slot_ToDelete)
                        {
                            gm.Slots.RemoveAt(i);
                        }
                    }

                }
            }
        }
    }

    public static IEnumerator ConstructBuilding(Building currentBuilding, int workersHere, bool StepByStep)
    {
        currentBuilding.constructInProgress = true;
        currentBuilding.buildCurrentProgress += workersHere;
        if (StepByStep == true)
        {
            yield return new WaitForSeconds(0.01f);
        }else
        {
            yield return new WaitForSeconds(1);
        }
        currentBuilding.constructInProgress = false;
    }

    public void ReorganizePeopleTransfers()
    {
        foreach (PeopleTransfer transfer in gm.PeoplesTransferList)
        {
            if (transfer.timeRestTransfer > 0)
            {
                transfer.timeRestTransfer--;
            }
            else
            {
                transfer.peopleTransferred.people_GameObject.transform.SetParent(transfer.whomTransfer.region_PeoplesDock.transform);
                transfer.peopleTransferred.people_GameObject.transform.localScale = new Vector3(1,1,1);
                transfer.whomTransfer.Peoples.Add(transfer.peopleTransferred);
                transfer.peopleTransferred.people_RegionWherePlaced = transfer.whomTransfer;
                transfer.peopleTransferred.people_InTransfer = false;
                transfer.toDelete = true;
            }
        }
        for (int i = (gm.PeoplesTransferList.Count - 1); i >= 0; i--)
        {
            if (gm.PeoplesTransferList[i].toDelete)
            {
                gm.PeoplesTransferList.RemoveAt(i);
            }
        }

        if (gm.CurrentPeoplePersonal != null)
        {
            OverPeople.Get_People_Form_GameObject(gm.CurrentPeoplePersonal);
        }
    }

}
