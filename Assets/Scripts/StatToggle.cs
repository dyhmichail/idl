﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StatToggle : MonoBehaviour {

	public void StatsTogglePressed(){
		int state = this.GetComponent<Toggle>().isOn == true ? 1 : 0;
		PlayerPrefs.SetInt(this.name, state);
		gm.RefreshStats1Window();
	}
}
