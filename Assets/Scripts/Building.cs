﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Building {
	public static int current_ID = 1;
	public int building_ID;
	public string building_Name;
	public GameObject building_GameObject;
	public BuildingType building_Type;
	public Region building_RegionWherePlaced;
	public GameObject building_PeoplesDock;
    public bool construct_Finished;

	public int slotsFilled;
	public int seatsFilled;
	public int seatsReserved;

    //Construct section
	public int contructionWorkerHere = 0;
	public bool constructionSlotsRequested = false;
	public int constructionProgress = 0;
    public int buildCurrentProgress = 0;
    public GameObject ProgressBar_Under;
    public GameObject ProgressBar;
    public Color spriteColor;
    public bool constructInProgress = false;
    
    public Dictionary<GoodType, int> whatNeedToConstructStored = new Dictionary<GoodType, int>();

    public Building(GameObject _building_GameObject, BuildingType _building_Type, Region _building_RegionWherePlaced)
    {
		building_GameObject = _building_GameObject;
		building_Type = _building_Type;
		building_RegionWherePlaced = _building_RegionWherePlaced;
		building_ID = current_ID++;
		building_Name = building_Type.building_Name + "_"+ building_ID.ToString("000");
		building_GameObject.name = building_Name;
	    foreach (KeyValuePair<GoodType, int> pairNeed in building_Type.need_4_Build)
	    {
	        whatNeedToConstructStored.Add(pairNeed.Key,0);
	    }
		seatsFilled = 0;
		slotsFilled = 0;
		seatsReserved = 0;
		building_PeoplesDock = this.building_GameObject.transform.FindChild("Canvas").FindChild("Panel_PeoplesDock").gameObject;
	}

	public static Building Add_Building(Region region, BuildingType buildingType, bool constructBuilding, GameObject colliderToBuild){
		GameObject tempBuilding = null;
		Building tempBuildingClass = null;

	    GameObject new_Building = null;
	    if (colliderToBuild == null)
	    {
	        new_Building = Get_Free_Collider_For_Construct(region);
	    }
	    else
	    {
	        new_Building = colliderToBuild;
	    }

		if (new_Building != null){

            new_Building.GetComponent<SpriteRenderer>().sprite = Resources.Load("Images/Buildings/" + buildingType.building_Name, typeof(Sprite)) as Sprite;
            new_Building.GetComponent<Construct_Mouse_Over_Collider>().startConstruct = true;

		    if (constructBuilding == false)
		    {
		        new_Building.GetComponent<Construct_Mouse_Over_Collider>().finished = true;
		    }
		    tempBuildingClass = new Building(new_Building, buildingType, region);
            tempBuildingClass.ProgressBar_Under = new_Building.gameObject.transform.Find("Canvas").Find("Image_Progress_Under").gameObject;
            tempBuildingClass.ProgressBar = new_Building.gameObject.transform.Find("Canvas").Find("Image_Progress").gameObject;

		    if (constructBuilding == false)
		    {
		        tempBuildingClass.construct_Finished = true;
		        tempBuildingClass.ProgressBar_Under.GetComponent<Image>().fillAmount = 0;
		    }
		    else
		    {
                tempBuildingClass.construct_Finished = false;
		        tempBuildingClass.constructionSlotsRequested = false;
		    }


		    region.Buildings.Add(tempBuildingClass);
		}
		return tempBuildingClass;
	}
    
	public static GameObject Get_Free_Collider_For_Construct(Region regionWhere){
		foreach(GameObject G in gm.Regions[regionWhere.region_Name].Colliders_4_Build){
			if (G.GetComponent<Construct_Mouse_Over_Collider>().startConstruct == false){
				return G;
			}
		}
		return null;
	}

    public static Region Get_Region_With_Free_Collider_For_Build()
    {
        foreach (KeyValuePair<string, Region> pairRegion in gm.Regions)
        {
            if (Get_Free_Collider_For_Construct(pairRegion.Value) != null)
            {
                return pairRegion.Value;
            }
        }
        return null;
    }

	public static bool Building_Produce_Protection(Building currentBuilding){
		foreach(GoodType currentGood in currentBuilding.building_Type.building_produce.Keys){
			if(currentGood.good_Name == "protection"){
				return true;
			}
		}
		return false;
	}
    
    public static bool All_Mats_To_Build_Present_2(BuildingType currentBuilding, Region currentRegion) {
        bool readyToBuild = true;
        foreach (KeyValuePair<GoodType, int> pair in gm.typeOfBuilding[currentBuilding.building_Name].need_4_Build)
        {
            if (gm.typeOfBuilding[currentBuilding.building_Name].need_4_Build[pair.Key] > currentRegion.warehouse_Reserved[pair.Key])
            {
                readyToBuild = false;
            }
        }
        return readyToBuild;
    }

    public static void Remove_Mats_To_Build_2(BuildingType currentBuilding, Region currentRegion){
		foreach(KeyValuePair<GoodType, int> pair in gm.typeOfBuilding[currentBuilding.building_Name].need_4_Build){
		    currentRegion.warehouse_Reserved[pair.Key] -= gm.typeOfBuilding[currentBuilding.building_Name].need_4_Build[pair.Key];
		}
	}

}

public class BuildingType{
	public int building_ID;
	public string building_Name;
	public bool building_Real;
	public int seatsQuantity;
	public int time_to_build;
    public int building_Slot;
    public int builders_Need;

	public Dictionary<GoodType, int> need_4_Build = new Dictionary<GoodType, int>();
	public Dictionary<GoodType, int> building_produce = new Dictionary<GoodType, int>();

	public BuildingType(int 	 _building_ID,
	                    string	 _building_Name,
	                    bool	 _building_Real,
	                    int 	 _seatsQuantity,
	                    int		 _time_to_build,
                        int      _building_Slot,
                        int      _builders_Need
	                    )
	{
		building_ID		= _building_ID;
		building_Name	= _building_Name;
		building_Real	= _building_Real;
		seatsQuantity	= _seatsQuantity;
		time_to_build	= _time_to_build;
	    building_Slot   = _building_Slot;
	    builders_Need   = _builders_Need;
	}

}
