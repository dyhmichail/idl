﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public class SlotType {
	
	public int slot_ID;
	public string slot_Name;
	public int slot_r, slot_g, slot_b;

	public SlotType(int _slot_ID, string _slot_Name, int _slot_r, int _slot_g, int _slot_b){
		slot_ID = _slot_ID;
		slot_Name = _slot_Name;
		slot_r = _slot_r;
		slot_g = _slot_g;
		slot_b = _slot_b;
	}
	
}

public class Slot {
	public static int current_ID = 1;
	public int slot_ID;
	public string slot_Name;
	public SlotType slot_Type;
	public GameObject slot_GameObject;
	public Building slot_BuildingWhereAssigned = null;
	public People slot_PeopleAssigned = null;
	public bool slot_Reserved;
	public bool slot_ToDelete = false;
	
	public Slot(GameObject _slot_GameObject, SlotType _slot_Type){
		slot_ID = current_ID++;
		slot_Type = _slot_Type;
		slot_Name = slot_Type.slot_Name + "_" + slot_ID.ToString();
		slot_GameObject = _slot_GameObject;
		slot_GameObject.name = slot_Name;
		slot_Reserved = false;
	}


	public static Slot Add_Slot(SlotType slotType, bool allowMultiple){
		GameObject tempSlot = null;
		Slot tempSlotClass = null;
		bool needExists = false;
		
		foreach(Slot slot in gm.Slots){
			if ((slot.slot_BuildingWhereAssigned == null) || (slot.slot_PeopleAssigned == null)){
				if (slot.slot_Type == slotType){
					needExists = true;
				}
			}
		}

	    if ((needExists == false) || allowMultiple)
	    {

	        tempSlot = Resources.Load("Prefab/Image_Slot", typeof (GameObject)) as GameObject;
	        tempSlot.transform.Find("Text").GetComponent<Text>().text = slotType.slot_Name.Substring(0, 1).ToUpper();
	        tempSlot.GetComponent<Image>().color = new Color(slotType.slot_r, slotType.slot_g, slotType.slot_b, 1);

	        GameObject new_Slot = GameObject.Instantiate(tempSlot);
	        tempSlotClass = new Slot(new_Slot, slotType);
	        gm.Slots.Add(tempSlotClass);
	        new_Slot.transform.SetParent(gm.Panel_Slots.transform);
	        new_Slot.transform.localScale = new Vector3(1, 1, 1);
	        new_Slot.transform.localPosition = new Vector3(0, 0, 0);

	        return tempSlotClass;
	    }
	    return null;
	}

	public static SlotType Get_Slot_Type_On_ID(int current_slot_ID){
		foreach(KeyValuePair<string, SlotType> pairSlotType in gm.typeOfSlot){
			if(pairSlotType.Value.slot_ID == current_slot_ID){
				return pairSlotType.Value;
			}
		}
		return null;
	}

    public static BuildingType Get_Building_For_SlotType(SlotType currentSlot)
    {
        foreach (KeyValuePair<string, BuildingType> pairBuilding in gm.typeOfBuilding)
        {
            //Debug.Log(pairBuilding.Value.building_Slot + "|" + currentSlot.slot_ID + "|" + currentSlot.slot_Name);
            if (pairBuilding.Value.building_Slot == currentSlot.slot_ID)
            {
                //Debug.Log("Here!");
                return pairBuilding.Value;
            }
        }
        return null;
    }

}

