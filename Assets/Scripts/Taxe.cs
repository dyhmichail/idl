﻿using UnityEngine;
using System.Collections;

public class Taxe : MonoBehaviour {

    public static int current_ID = 1;
	public int taxe_ID;
    public int taxe_region;
    public int taxe_product;
    public int taxe_value;

    public Taxe(int _taxe_region, int _taxe_product, int _taxe_value)
    {
        taxe_ID = current_ID++;
        taxe_region = _taxe_region;
        taxe_product = _taxe_product;
        taxe_value = _taxe_value;
    }
}
