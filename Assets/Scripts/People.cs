﻿using UnityEngine;
using System.Collections;

public class People {

	public static int current_ID = 1;
	public int people_ID;
	public string people_Name;
	public Slot people_Slot = null;
	public Region people_RegionWherePlaced;
	public bool people_InTransfer = false;
	public bool people_ToDelete = false;
	public bool people_InArmyNow = false;
    public GameObject people_GameObject;
    public Troop people_Troop;


	public int food_limit; 
	public int food_limit_now; 
	public int[] food_limit_random = new int[2]; 

	public int protect_limit; 
	public int protect_limit_now; 
	public int[] protect_limit_random = new int[2]; 
	
	public int goods_limit; 
	public int goods_limit_now; 
	public int[] goods_limit_random = new int[2]; 	

	public People(GameObject _people_GameObject, Region _people_RegionWherePlaced, bool _people_InTransfer){
		people_GameObject = _people_GameObject;
		people_RegionWherePlaced = _people_RegionWherePlaced;
		people_InTransfer = _people_InTransfer;
		people_ID = People.current_ID++;
		people_Name = "People_" + people_ID.ToString("000");
		people_GameObject.name = people_Name;

		food_limit_now = gm.food_limit_now; 
		food_limit_random[0] = gm.food_limit_random_min;
		food_limit_random[1] = gm.food_limit_random_max;
	    food_limit = Random.Range(gm.food_limit_random_min, gm.food_limit_random_max);
		
		protect_limit_now = gm.protect_limit_now; 
		protect_limit_random[0] = gm.protect_limit_random_min;
		protect_limit_random[1] = gm.protect_limit_random_max;
	    protect_limit = Random.Range(gm.protect_limit_random_min, gm.protect_limit_random_max);
		
		goods_limit_now = gm.goods_limit_now; 
		goods_limit_random[0] = gm.goods_limit_random_min;
		goods_limit_random[1] = gm.goods_limit_random_max; 	
		goods_limit = Random.Range(gm.goods_limit_random_min, gm.goods_limit_random_max);
	}

	public static People Add_People(Region region, bool inTransfer){
		GameObject tempPeople = null;
		People tempPeopleClass;
		tempPeople = Resources.Load("Prefab/Image_People_Free", typeof(GameObject)) as GameObject;
		GameObject new_People = GameObject.Instantiate(tempPeople) as GameObject;
		tempPeopleClass = new People(new_People, region, inTransfer);
		region.Peoples.Add(tempPeopleClass);
	    new_People.transform.SetParent(region.region_PeoplesDock.transform);
		new_People.transform.localScale = new Vector3(1,1,1);
		return tempPeopleClass;
	}
}
