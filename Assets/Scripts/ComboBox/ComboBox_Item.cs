﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ComboBox_Item : MonoBehaviour {
	public Text button_Text;
	public int choosedItem;
	public GameObject button_Main;
	public GameObject ComboBox_Main;


	public event Sql_Init.ChangeRegionName onChangeRegion;


	void Start(){
		onChangeRegion += new Sql_Init.ChangeRegionName(ComboBox_Main.GetComponent<ComboBox_Full>().Change_Region_Name);
	}


	public void ItemClick(){
		if(onChangeRegion != null){
			onChangeRegion(this.button_Text.text.Trim());
		}
	}


}
