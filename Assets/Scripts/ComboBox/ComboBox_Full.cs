﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ComboBox_Full : MonoBehaviour {

	public List<GameObject> Items = new List<GameObject>();
	public GameObject Main_GameObject;
	public GameObject Panel_Items;
	public GameObject Button_Main;

	public Image ArrowImage;
	public int choosedItem;
	public string choosedItemString;
	public Text Text_regionName;

	void Start(){
		GenerateGotoComboBox();
	}

	public void GenerateGotoComboBox(){
		foreach (KeyValuePair<string, Region> pair in gm.Regions){
			if(pair.Value.region_Real == true){
				AddItem(pair.Value.region_ID, pair.Key);
			}
		}
		HideCurrentRegionButton(gm.current_Region);
		Panel_Items.SetActive(false);
	}

	public void AddItem(int itemID, string itemName){
		GameObject tempGameObject = Resources.Load("Prefab/ComboBox/Button_Item_", typeof(GameObject)) as GameObject;
		GameObject new_Element = GameObject.Instantiate(tempGameObject) as GameObject;
		new_Element.transform.SetParent(Panel_Items.transform);
		new_Element.name = "Item_" + itemName;
		new_Element.GetComponent<ComboBox_Item>().button_Text.text = "  " + itemName;
		new_Element.GetComponent<ComboBox_Item>().choosedItem = itemID;
		new_Element.GetComponent<ComboBox_Item>().button_Main = Button_Main;
		new_Element.GetComponent<ComboBox_Item>().ComboBox_Main = Main_GameObject;
		new_Element.transform.localScale = new Vector3(1,1,1);
		Items.Add(new_Element);
	}

	public void ClickMain(){
		if(Panel_Items.activeSelf == true){
			Panel_Items.SetActive(false);
			ArrowImage.transform.localScale = new Vector3(1f,1f,1);
		} else {
			Panel_Items.SetActive(true);
			ArrowImage.transform.localScale = new Vector3(1f,-1f,1);
		}
	}

	public void Change_Region_Name(string regionName){
		Text_regionName.text = regionName;
		choosedItem = gm.Regions[regionName].region_ID;
		choosedItemString = regionName;
		//Button_Main.GetComponentInChildren<Text>().text = " " + regionName;
		Panel_Items.SetActive(false);
		ArrowImage.transform.localScale = new Vector3(1f,1f,1f);
		gm.Change_Region_GameObject(regionName);
		gm.current_Region = regionName;
		HideCurrentRegionButton(regionName);
        gm.Refresh_Warehouse_Panel(false);
	}

	public void HideCurrentRegionButton(string currentRegion){
		foreach(GameObject G in Items){
			G.SetActive(true);
			if (G.name == "Item_" + currentRegion){
				G.SetActive(false);
			}
		}
	}

}
