﻿using UnityEngine;
using System.Collections;

public class Product : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

public class ProductType{
	public int product_ID;
	public string product_Name;
	public bool product_Real;

	public ProductType(int		_product_ID,
	                   string	_product_Name,
	                   bool		_product_Real
	                   )
	{
		product_ID			= _product_ID;
		product_Name		= _product_Name;
		product_Real		= _product_Real;
	}
}