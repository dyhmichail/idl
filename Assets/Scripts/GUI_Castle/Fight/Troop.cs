﻿

using UnityEngine;
using System.Collections;

public class Troop{

	public enum typesOfTroops {Archer = 1, Fighter = 2,  Mage = 3};
	public enum typesOfWounds {Healthy = 1, LightWound = 2, HardWound = 3, Die = 4};
	
	public static int current_ID = 1;
	public int troop_ID;
	public string troop_Name;
	public typesOfTroops troop_Type;
	public GameObject troop_GameObject;
	public bool ourOrEnemy;
    public People troop_Owner;
	
	public Troop(GameObject _troop_GameObject, typesOfTroops _troop_Type, bool _ourOrEnemy, People _troop_Owner){
		troop_ID = current_ID++;
		troop_Type = _troop_Type;
		troop_Name = troop_Type.ToString() + troop_ID.ToString();
		ourOrEnemy = _ourOrEnemy;
		troop_GameObject = _troop_GameObject;
		troop_GameObject.name = troop_Name + (ourOrEnemy == true ? "_Our" : "_Enemy").ToString();
	    troop_Owner = _troop_Owner;
	}

}
