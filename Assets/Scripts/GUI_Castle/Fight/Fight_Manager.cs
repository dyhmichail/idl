﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Fight_Manager : MonoBehaviour {

	public static List<Troop> OurTroops = new List<Troop>();
	public static List<Troop> EnemyTroops = new List<Troop>();
	public int averageTroopInArmy;
	public int randomAdditionToArmy;
	public float timeToMove = 10f;
	public static GameObject DiceResult;
	public static System.Random rnd = new System.Random();
	public static float startPosition = 5f;
    public static GameObject Button_Fight_Start;
    public static GameObject Panel_Results;
    public static Text Text_Main_Result;
    public static Text Text_Trophy;
    public static GameObject Fighter_Repository;

	void Start(){

        Button_Fight_Start = GameObject.Find("Button_Fight_Start");
        Panel_Results = GameObject.Find("Panel_Results");
        Text_Main_Result = GameObject.Find("Text_Main_Result").GetComponent<Text>();
        Text_Trophy = GameObject.Find("Text_Trophy").GetComponent<Text>();
        Fighter_Repository = GameObject.Find("Fighter_Repository");

        Panel_Results.SetActive(false);

		averageTroopInArmy = gm.min_army;
		randomAdditionToArmy = 2;


		for(int i = 0; i < averageTroopInArmy; i++){
            gm.PeoplesReadyToWar[i].people_Troop = Add_Troop((Troop.typesOfTroops)Random.Range(1, System.Enum.GetNames(typeof(Troop.typesOfTroops)).Length + 1), true, new Vector3(-startPosition + i * 0.8f, -1.5f, 0f), gm.PeoplesReadyToWar[i]);
		}

		int tempRandom = rnd.Next(0, randomAdditionToArmy);

		for(int i = 0; i < (averageTroopInArmy + tempRandom); i++){
			Add_Troop((Troop.typesOfTroops)Random.Range(1, System.Enum.GetNames(typeof(Troop.typesOfTroops)).Length + 1), false, new Vector3(startPosition - i * 0.8f, -1.5f, 0f), null);
		}
	}


	public Troop Add_Troop(Troop.typesOfTroops troopType, bool ourOrEnemy, Vector3 troopPosition, People troopOwner){
		Troop tempTroopClass = null;
		GameObject tempTroop = null;

		switch (troopType){
			case Troop.typesOfTroops.Archer:
				tempTroop = Resources.Load("Prefab/Fight/Archer", typeof(GameObject)) as GameObject;
				break;
			case Troop.typesOfTroops.Fighter:
				tempTroop = Resources.Load("Prefab/Fight/Fighter", typeof(GameObject)) as GameObject;
				break;
			case Troop.typesOfTroops.Mage:
				tempTroop = Resources.Load("Prefab/Fight/Mage", typeof(GameObject)) as GameObject;
				break;
		}

		GameObject new_Troop = Instantiate(tempTroop);

		new_Troop.GetComponent<Fight>().t_Type = troopType;
		tempTroopClass = new Troop(new_Troop, troopType, ourOrEnemy, troopOwner);
		if (ourOrEnemy == true){
			OurTroops.Add(tempTroopClass);
			new_Troop.layer = 8;
			tempTroopClass.troop_GameObject.GetComponent<Fight>().OurOrEnemy.GetComponent<SpriteRenderer>().color = Color.green;
			new_Troop.GetComponent<Fight>().ourOrEnemy = true;
		} else {
			EnemyTroops.Add(tempTroopClass);
			new_Troop.layer = 9;
			tempTroopClass.troop_GameObject.GetComponent<Fight>().OurOrEnemy.GetComponent<SpriteRenderer>().color = Color.red;
			new_Troop.GetComponent<Fight>().ourOrEnemy = false;
			new_Troop.transform.localScale = new Vector3(-new_Troop.transform.localScale.x , new_Troop.transform.localScale.y,1);
		}

        new_Troop.transform.SetParent(gm.Fight_Main.transform);
        new_Troop.transform.localPosition = troopPosition;

		return tempTroopClass;
	}


	public void FightStart(){
		float finalPosX = startPosition;
		float timeToMoveCurrent = timeToMove;
		float pathSize = startPosition + finalPosX;

		foreach(Troop troop in OurTroops){
			timeToMoveCurrent = Mathf.Abs(troop.troop_GameObject.transform.position.x) * timeToMove / (float)pathSize;
			iTween.MoveTo(troop.troop_GameObject.gameObject, iTween.Hash("x", finalPosX, "time", timeToMoveCurrent, "ignoretimescale", false, "easetype", "easeInSine"));
		}

		foreach(Troop troop in EnemyTroops){
			timeToMoveCurrent = Mathf.Abs(troop.troop_GameObject.transform.position.x) * timeToMove / (float)pathSize;
			iTween.MoveTo(troop.troop_GameObject.gameObject, iTween.Hash("x", -finalPosX, "time", timeToMoveCurrent, "ignoretimescale", false, "easetype", "easeInSine"));
		}
	}


    // 0 - fight, 1 - Our win, 2 - Enemy Win
    public static int CheckWin()
    {
        bool liveFighter = false;
        foreach (Troop troop in OurTroops)
        {
            if (troop.troop_GameObject.GetComponent<Fight>().activeTroop == true)
            {
                liveFighter = true;
            }
        }
        if (liveFighter != true)
        {
            return 2;
        }
        liveFighter = false;
        foreach (Troop troop in EnemyTroops)
        {
            if (troop.troop_GameObject.GetComponent<Fight>().activeTroop == true)
            {
                liveFighter = true;
            }
        }
        if (liveFighter != true)
        {
            return 1;
        }
        return 0;
    }


    public static void Show_Fight_Results(int whoWin)
    {
        Button_Fight_Start.SetActive(false);
        Panel_Results.SetActive(true);
        if (whoWin == 1)
        {
            Text_Main_Result.text = "You WIN!!!";

            foreach(Troop troop in OurTroops)
            {
                if ( troop.troop_Owner != null)
                {
                    gm.PeoplesReadyToWar.Remove(troop.troop_Owner);
                    troop.troop_Owner.people_Troop = null;
                    troop.troop_Owner.goods_limit_now = 0;
                }
            }

        }
        else
        {
            Text_Main_Result.text = "You LOSE!!!";

            foreach (var currentPeople in gm.PeoplesReadyToWar)
            {
                if (currentPeople.people_Troop != null)
                {
                    currentPeople.people_RegionWherePlaced.Peoples.Remove(currentPeople);
                    currentPeople.people_RegionWherePlaced = null;
                    currentPeople.people_GameObject.transform.SetParent(gm.PanelLeavedPeoples.transform);
                    currentPeople.people_GameObject.transform.localScale = new Vector3(1, 1, 1);
                }
            }

        }
        Text_Trophy.text = Calculate_Trophy(whoWin, 10f);
        Put_All_Fighters_In_Repository();
    }

    public static void Resort_People_After_Fight()
    {
    }

    public static void Put_All_Fighters_In_Repository()
    {
        foreach (Troop troop in OurTroops)
        {
            troop.troop_GameObject.transform.SetParent(Fighter_Repository.transform);
        }
        foreach (Troop troop in EnemyTroops)
        {
            troop.troop_GameObject.transform.SetParent(Fighter_Repository.transform);
        }
    }

    public static string Calculate_Trophy(int whoWin, float percentageOfTrophy)
    {
        string trophy_String = "\nTrophy:";
        
        foreach (KeyValuePair<string, GoodType> pairGood in gm.typeOfGood)
        {
            if (pairGood.Value.good_Real == true)
            {
                int currentTrophy =
                    Mathf.RoundToInt(gm.Regions["Castle"].warehouse_Reserved[pairGood.Value]/percentageOfTrophy);
                    trophy_String += "\n" + pairGood.Value.good_Name + " | " + currentTrophy.ToString();

                if (whoWin == 1)
                {
                    gm.Regions["Castle"].warehouse_Reserved[pairGood.Value] += currentTrophy;
                }
                else
                {
                    gm.Regions["Castle"].warehouse_Reserved[pairGood.Value] -= currentTrophy;
                }
            }
        }
        return trophy_String;
    }

}
