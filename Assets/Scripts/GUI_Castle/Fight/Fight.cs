﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Fight : MonoBehaviour {


	private Animator animator;
	public GameObject OurOrEnemy, SignalCollision;
	public bool ourOrEnemy;
	public Troop.typesOfTroops t_Type;
	public bool activeTroop = true;
	public static GameObject iam,opponent;
	public bool firstDiceResult;
	public Color color = Color.white;
	public static bool inFight = false;

	void Awake(){
		animator = GetComponent<Animator> ();
		color.a = 0;
	}


	void OnTriggerEnter2D(Collider2D coll) {

		if (inFight == false){
			if ((coll.GetComponent<Fight>().activeTroop != false) && (this.GetComponent<Fight>().activeTroop != false)){
				this.SignalCollision.gameObject.GetComponent<SpriteRenderer>().color = Color.blue;
				coll.gameObject.GetComponent<Fight>().SignalCollision.gameObject.GetComponent<SpriteRenderer>().color = Color.blue;
			}

			if ((this.gameObject.layer == 8) && (coll.gameObject.layer == 9)){
				if ((coll.GetComponent<Fight>().activeTroop != false) && (this.GetComponent<Fight>().activeTroop != false)){
					inFight = true;
					StopAllMoving();
					Debug.Log(this.name + " | " + coll.name + " | " + this.gameObject.layer.ToString());

					iam = this.gameObject;
					opponent = coll.gameObject;

					firstDiceResult = firstDice();

					if (firstDiceResult == true){
						opponent.GetComponent<Fight>().activeTroop = false;
						iam.GetComponent<Fight>().animator.SetBool("attack", true);
					} else {
						iam.GetComponent<Fight>().activeTroop = false;
						opponent.GetComponent<Fight>().animator.SetBool("attack", true);
					}

				    if (Fight_Manager.CheckWin() != 0)
				    {
				        Fight_Manager.Show_Fight_Results(Fight_Manager.CheckWin());
				    }
				}
			}
		}
	}

	public bool firstDice(){
		return Fight_Manager.rnd.Next(100) > 50 ? true : false;
	}

	public void downOrNot(){
		opponent.GetComponent<Fight>().SignalCollision.gameObject.GetComponent<SpriteRenderer>().color = color;
		iam.GetComponent<Fight>().SignalCollision.gameObject.GetComponent<SpriteRenderer>().color = color;
		if (firstDiceResult == true){
			opponent.GetComponent<Fight>().animator.SetBool("down", true);
			iam.GetComponent<Fight>().animator.SetBool("attack", false);
		} else {
			iam.GetComponent<Fight>().animator.SetBool("down", true);
			opponent.GetComponent<Fight>().animator.SetBool("attack", false);
		}
	}

	public void StartAllMoving(){
		inFight = false;
		Debug.Log (">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		foreach(Troop troop in Fight_Manager.OurTroops)	{
			if (troop.troop_GameObject.GetComponent<Fight>().activeTroop == true) {
				troop.troop_GameObject.GetComponent<iTween>().enabled = true;
			}
		}
		foreach(Troop troop in Fight_Manager.EnemyTroops){
			if (troop.troop_GameObject.GetComponent<Fight>().activeTroop == true){
				troop.troop_GameObject.GetComponent<iTween>().enabled = true;
			}
		}
	}

	public void StopAllMoving(){
		foreach(Troop troop in Fight_Manager.OurTroops){
			troop.troop_GameObject.GetComponent<iTween>().enabled = false;
		}
		foreach(Troop troop in Fight_Manager.EnemyTroops){
			troop.troop_GameObject.GetComponent<iTween>().enabled = false;
		}
	}

}