﻿using UnityEngine;
using System.Collections;

public class Button_Bottom : MonoBehaviour
{

    public static int current_Button_ID = 0;
    public int button_ID;

    public void Press_Bottom_Button()
    {
        current_Button_ID = button_ID;
        gm.RearrangeBottomButtonsAndPanels(current_Button_ID);
    }
}
