﻿using UnityEngine;
using System.Collections;

public class CheckAllButtons : MonoBehaviour
{

    public bool checkUnCheck;
    public GameObject panelWithToggles;

    public void PressButton()
    {
        gm.CheckAllToggles(panelWithToggles, checkUnCheck);
    }

}
